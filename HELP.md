# HELP / FAQ

## Basic Commands

!me
!props
!buy

## Stream Jester

What is the Stream Jester?

How do I come the Stream Jester?

What can I do as Jester?

## Playing Sounds

- Streamgods can play all sounds, without costing Mana, or affecting the price
  of soundeffects

- Stream Jesters can play all sounds, but it costs them 1 Mana.

- All other users have to own the sound, and they can then play the sound for 1
  Mana.

## Commands

- !love / !hate
  - You can love +1 person per how many love you.
  - +1 for each lover for your street cred
  - default is +3
- !lovers
  - Seeing who you love, who loves you
- !buy
  - !buy
  - !buy 20  -> the max to show all the names in Twitch
  - !buy 100 -> more than 20 it shows the amount and Cool Points
- !perms
  - !perms damn
  - !perms zanuss
- !props
  - !props
  - !props 10
  - !props @zanuss
- !soundeffect
  -> should be working now, and better
- !me
  - if you want your page refreshed !me
  - if you get access denied, wait a second and refresh (!me triggers generation
    and upload of the page)
- !color
- !pokemon
- !guess
- !props
