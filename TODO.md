# TODO

## Weekly Goals

- Selling / Giving of Commands
- Start of Plan for Deployed DB/Website -> OBS/SFX link (How to split and
  communicate)
  - Plan For API
- Parties started to be coded
- Running Tests on Gitlab CI
- Better Basic Website
  - Better Help
  - Better Overall structure
  - Better Updating
  - M I N I M A L
  - A E S E T I C
  - Switch to beginworld.exchange
    - HTTPS
    - /index.html redirection
    - better 404
- Pulling in Gifs and Videos with some fun manipulation
  -> Get the basic of effects working, for at least stream lords
  -> Sorry! This is stream lords only!
  -> We will have stream lord try-outs soon!
- Logging of Events, soo many events
  -> Helpful for a million things
- Finish Insurance
- Finish Soundpacks
- Love / Hate sounds

## Other

- Fix Cubing
- Maybe Figure out News Again (Based on Logging of Events)
- Bring Back ISASIP
- !dropeffect damn 10
- !buy all
  - it looks for "all" command, doesn't find it,
    so it gives you a random command
!colorbrown
!highlight

!dropeffect brown_bit colorbrown

## Failed Features Finish

- Rubix Cube Gambling
- Custom CSS for Pages
  - DNS for Linode
- Revolution/Coup

- Soundpacks Packs
- Loving / Hating Sounds
- Insurance
- Donate / Give / Share

## Top TODOs

- User CSS Working again

# Bugs

- Some themes songs are being returned from !buy maybe
- check if sound already exists, and warn the user

## DB Design

- Re-add insurance
- Revolution/Coup
- Cubin
- Donate / Give / Share
- Sell System
- Get packs working
  - Have to structure the DB as well
- Add Loving / Hating Sounds
- Build out DB table to track all interactions for auditing, debugging

### Insurance

- just a boolean on a User?
  -> Super simple

### Revolution / Coup

Votes Table
  -> revolution / coup
  - Player_id

- You must vote, one at a time!
  - Spam another channel: beginbotbot channel

## Help Topics

- New WTF Video
- better !help command
- !help
- !commands
- Linode Explanation

## Vague Requests / Ideas

- Potentially add more constraints to ChatMessage, only allow NUll MESSAGES
- Mark more things not to remove mana
- Move more commands go to beginbotbot
- Build an admin channel to be able to silence any sounds easily

## Website Local/Linode

- Add user requests html generation
- Create a Recently Added sounds section
- Stats/Events/Errors
- Chat Messages
- New Sounds
- Soundeffect Requests
- See current audio requests
  - How long until the sound will be played
  - How many in line
- Review command pages
  - get links for user page
  - get sounds sync
  - Make sure they are being uploaded often
- Full website Generation for Every page

## Small Todos

- Countdown on the cat timer
- !buy all
- Migrate the bots over and have them ignored for jester stuff
  - Build bot logic into more place and link bots to their owners
- Keep track of valid guess for pokemon
  -> also tell the people if the guess was even valid

## Feature Requests

- !yt puppy
- !yt puppy http://jdsfhlgksjdhflg
  - We have to backfill
- !lock
  - lock you some position
- Allow chat to control audio sync/desync

## Tech Debt

- Easier to know all messages beginbotbot is sending back, and whether they are considered to be successes

## Ideas

- Theme Song Guessing Game
- Investing in Sounds

### Linode Next Steps

- Instance
- Generate SSH Key
- Create Packer instance

## House Keeping

- Look at Approvals
- Migration to find what sounds aren't in the linode bucket
- Migration to find soundeffects that don't have corresponding filenames
- Migration to Clean Up Mana and money

## Potentially Working

- Make sure websockets notifications are working
- !me need to make sure website is begin updated
- Auto-approving for soundeffects, for stream lords, gods
- Auto allow users their own theme songs / Submitted sounds
- !jester -> show who is jester
  - allow explict choosing of jester
- !me need to make sure website is begin updated
- deny sounds that already exist, with a message

## Reported Bugs

@beginbot found a bug on the props command, when you don't have enough street cred to give the amount you wanted it says you have 0 instead of your real amount

