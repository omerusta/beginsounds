CREATE TABLE public.players(
    id SERIAL,
    name character varying(255) NOT NULL UNIQUE,
    streamlord bool DEFAULT false NOT NULL,
    streamgod bool DEFAULT false NOT NULL,
    bot bool DEFAULT false NOT NULL,
    mana int DEFAULT 0 NOT NULL,
    street_cred int DEFAULT 0 NOT NULL,
    cool_points int DEFAULT 0 NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT players_pkey PRIMARY KEY (id)
);
