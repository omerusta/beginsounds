CREATE TABLE public.chat_messages (
    id SERIAL,
    player_name character varying(255) NOT NULL,
    player_id int references players(id),
    message text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT chat_messages_pkey PRIMARY KEY (id)
);
