CREATE TABLE public.pokemon_answers(
    id SERIAL,
    pokemon character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    won_at timestamp with time zone,
    CONSTRAINT pokemon_answers_pkey PRIMARY KEY (id)
);
