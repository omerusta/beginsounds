CREATE TABLE public.commands_players (
    id SERIAL,
    player_id int references players(id),
    stream_command_id int references stream_commands(id),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT commands_players_pkey PRIMARY KEY (id),
    CONSTRAINT commands_players_unique UNIQUE(player_id,stream_command_id)
);
