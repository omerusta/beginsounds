package obs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type ObsCommand struct {
	Name  string `json:"name"`
	Type  string `json:"type"`
	Scene string `json:"scene"`
}

func (o *ObsCommand) String() string {
	return fmt.Sprintf(
		"ObsCommand<Name: %s Type: %s Scene: %s>",
		o.Name,
		o.Type,
		o.Scene,
	)
}

func parseConfig(configFile string) []ObsCommand {
	var results []ObsCommand
	// I could pass in the path
	// configFile, err := filepath.Abs("config/obs_commands.json")
	// configFile, err := filepath.Abs("../../config/obs_commands.json")
	f, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(f, &results); err != nil {
		panic(err)
	}
	fmt.Printf("results = %+v\n", results)
	return results
}
