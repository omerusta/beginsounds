package obs

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

func SlideInFromSide(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
) {
	ToggleSource(c, mutex, sceneName, sourceName, true)
	go func() {
		RotateSource(c, mutex, sceneName, sourceName, 90)
		// This should rotate first
		rand.Seed(time.Now().UnixNano())
		max := 500
		min := -100
		startX := 0.0
		endX := 275.0
		y := float64(rand.Intn(max-min) + min)
		SlideSource(c, mutex, sceneName, sourceName, startX, endX, y)
		<-time.NewTimer(time.Second * 1).C
		ReverseSlideSource(c, mutex, sceneName, sourceName, endX, startX, y, -1)
		// RotateSource(c, mutex, sceneName, sourceName, 0)
		// then unrotate
	}()
}
func floatSourceThroughScene(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
) {
	go func() {
		// We should rotate to be vertical
		ToggleSource(c, mutex, sourceName, sceneName, true)
		RotateSource(c, mutex, sceneName, sourceName, 0)

		rand.Seed(time.Now().UnixNano())
		max := 1400
		min := 200
		x := float64(rand.Intn(max-min) + min)
		startY := 600.0
		endY := -600.0
		RiseSource(c, mutex, sceneName, sourceName, startY, endY, x)
	}()
}

func AppearFromThinAir(c *obsws.Client, mutex *sync.Mutex, sourceName string) {
	filterName := "transform"
	ToggleSourceFilter(c, mutex, sourceName, "transform", true)

	start := -90.0
	end := 0.
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i < end; i += 0.01 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     0,
			"Filter.Transform.Rotation.X": i,
		}
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(
			sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
	}
	<-ticker.C
}

func ZoomFieldOfView(c *obsws.Client, mutex *sync.Mutex, sourceName string) {
	filterName := "transform"
	ToggleSourceFilter(c, mutex, sourceName, "transform", true)

	start := 179.0
	end := 75.0
	ticker := time.NewTicker(100 * time.Millisecond)

	// FetchFilterSettings: map[Commit:a0ce2960
	for i := start; i > end; i -= 0.02 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": i,
		}
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(
			sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
	}
	<-ticker.C
}

func GetSourcePosition(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
) {
	// req := obsws.NewGetSourceFilterInfoRequest
	// req := obsws.NewGetSceneItemPropertiesRequest(sceneName, sourceName)
	// req := obsws.NewSetSceneItemPositionRequest(sceneName, sourceName, x, y)
	// mutex.Lock()
	// if err := req.Send(*c); err != nil {
	// 	log.Print(err)
	// }
	// resp, err := req.Receive()
	// mutex.Unlock()
	// if err != nil {
	// 	log.Print(err)
	// }
	// fmt.Printf("resp = %+v\n", resp)
}

// SetSourcePosition - Set the position of a Source Position
func SetSourcePosition(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	x float64,
	y float64,
) {
	req := obsws.NewSetSceneItemPositionRequest(sceneName, sourceName, x, y)
	err := processMoveSceneItem(c, mutex, &req)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}
}

// ReverseSlideSource is used to reverse a slide
func ReverseSlideSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	startX float64,
	endX float64,
	y float64,
	modifier float64,
) {

	for x := startX; x > endX; x += modifier {
		fmt.Printf("x = %+v\n", x)
		SetSourcePosition(c, mutex, "codin", sourceName, x, y)
	}

}

func SlideSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	startX float64,
	endX float64,
	y float64,
) {
	xModifier := 0.8
	for x := startX; x < endX; x += xModifier {
		fmt.Printf("x = %+v\n", x)
		SetSourcePosition(c, mutex, "codin", sourceName, x, y)
	}
}

func RiseSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	startY float64,
	endY float64,
	x float64,
) {
	YModifier := 0.8
	for y := startY; y > endY; y -= YModifier {
		fmt.Printf("y = %+v\n", y)
		SetSourcePosition(c, mutex, "codin", sourceName, x, y)
	}
}

func ZoomAndSpin3(
	c *obsws.Client,
	sourceName string,
	delay time.Duration,
	mutex *sync.Mutex,
) {

	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	done := make(chan bool)

	go func() {
		x := 0.0
		y := 0.0
		z := 0.0
		xModifier := -0.5
		yModifier := 0.8
		zModifier := -0.4

		for {
			select {
			case <-done:
				return
			case <-timer.C:
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    y,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": y,
					"Filter.Transform.Rotation.Z": z,
				}

				x = x + xModifier
				y = y + yModifier
				z = z + zModifier
				mutex.Lock()
				req := obsws.NewSetSourceFilterSettingsRequest(
					sourceName,
					filterName,
					filterSettings,
				)
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
				mutex.Unlock()

			}
		}
	}()
}

func ZoomAndSpin2(c *obsws.Client, sourceName string, delay time.Duration, mutex *sync.Mutex) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	done := make(chan bool)

	go func() {
		x := 0.0
		y := 0.0
		z := 0.0
		modifier := 0.7
		for {
			select {
			case <-done:
				return
			case <-timer.C:
				// ReturnToNormie(c, "922", mutex)
				// ReturnToNormie(c, "keyboard", mutex)
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    y,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": y,
					"Filter.Transform.Rotation.Z": z,
				}

				x = x + modifier
				y = y - modifier
				z = z + (modifier + 0.3)
				mutex.Lock()
				req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
				mutex.Unlock()

			}
		}
	}()

}
func ZoomAndSpin(c *obsws.Client, sourceName string, delay time.Duration, mutex *sync.Mutex) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	done := make(chan bool)

	go func() {
		x := 0
		for {
			select {
			case <-done:
				return
			case <-timer.C:
				// ReturnToNormie(c, "922", mutex)
				// ReturnToNormie(c, "keyboard", mutex)
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    x,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": x,
					"Filter.Transform.Rotation.Z": x,
				}

				x++
				mutex.Lock()
				req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
				mutex.Unlock()

			}
		}
	}()

}

func WideBegin(
	c *obsws.Client,
	sourceName string,
	duration time.Duration,
	mutex *sync.Mutex) {

	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * duration)

	done := make(chan bool)
	go func() {
		x := 1

		for {
			select {
			case <-done:
				return
			case <-timer.C:
				// We need to return to defaults here
				// ReturnToNormie(c, "922", mutex)
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
				}
				x = x + 4
				mutex.Lock()
				req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
				mutex.Unlock()

			}
		}
	}()

}

func TallBegin(c *obsws.Client, sourceName string, delay time.Duration, mutex *sync.Mutex) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 20)
	// timer := time.NewTimer(time.Millisecond * 463)

	done := make(chan bool)
	// min := 50
	// max := 75
	// return
	go func() {
		y := 1

		for {
			select {
			case <-done:
				return
			case <-timer.C:
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.Y": y,
				}
				y = y + 4
				mutex.Lock()
				req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
				mutex.Unlock()

			}
		}
	}()

}

func TheZoom(c *obsws.Client, sourceName string, delay time.Duration, mutex *sync.Mutex) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 7)

	done := make(chan bool)
	// min := 50
	// max := 75
	// return
	go func() {
		x := 0
		for {
			select {
			case <-done:
				return
			case <-timer.C:
				// ReturnToNormie(c, "922", mutex)
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)
				// x := rand.Intn(max-min) + min
				// y := rand.Intn(max-min) + min
				// z := rand.Intn(max-min) + min
				// y := x / 2
				// z := x / 2
				// filterSettings := map[string]interface{}{"Filter.Transform.Rotation.X": x}
				// filterSettings := map[string]interface{}{
				// 	"Filter.Transform.Rotation.X": x,
				// 	"Filter.Transform.Rotation.Y": x,
				// 	"Filter.Transform.Rotation.Z": x,
				// 	// "Filter.Transform.Rotation.Y": y,
				// 	// "Filter.Transform.Rotation.Z": z,
				// }
				// This scaling up is a zoom in a affect
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
					"Filter.Transform.Scale.Y": x,
				}

				// Zoom in Spin thang
				// y := rand.Intn(max-min) + min
				// z := rand.Intn(max-min) + min
				// filterSettings := map[string]interface{}{
				// 	"Filter.Transform.Scale.X":    x,
				// 	"Filter.Transform.Scale.Y":    x,
				// 	"Filter.Transform.Rotation.X": x,
				// 	"Filter.Transform.Rotation.Y": y,
				// 	"Filter.Transform.Rotation.Z": z,
				// }

				// Cool Wierd slow zoom in
				// filterSettings := map[string]interface{}{
				// 	"Filter.Transform.Scale.X":    x,
				// 	"Filter.Transform.Scale.Y":    x,
				// 	"Filter.Transform.Rotation.X": x,
				// 	"Filter.Transform.Rotation.Y": x,
				// 	"Filter.Transform.Rotation.Z": x,
				// }
				x++
				mutex.Lock()
				req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
				mutex.Unlock()

			}
		}
	}()
}

func DelaySpin(c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string, sourceName string,
	delay time.Duration,
	duration time.Duration,
) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)

	done := make(chan bool)
	go func() {
		delayStartTimer := time.NewTimer(time.Second * delay)
		<-delayStartTimer.C
		timer := time.NewTimer(time.Second * duration)

		x := 50
		for {
			select {
			case <-done:
				return
			case <-timer.C:
				// ReturnToNormie(c, "922", mutex)
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    x,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": x,
					"Filter.Transform.Rotation.Z": x,
				}
				x++
				mutex.Lock()
				req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
				mutex.Unlock()

			}
		}
	}()
}

func BigBrain(c *obsws.Client, mutex *sync.Mutex, sourceName string) {
	ToggleSourceFilter(c, mutex, sourceName, "transform", true)

	start := 0.00
	end := -50.5
	ticker := time.NewTicker(5 * time.Millisecond)

	for i := start; i > end; i -= 0.01 {
		res := FetchFilterSettings(c, mutex, sourceName, "transform")
		fmt.Printf("res = %+v\n", res)

		filterSettings := map[string]interface{}{
			// "Filter.Transform.Camera.FieldOfView": 53.27,
			"Filter.Transform.Camera":     1,
			"Filter.Transform.Position.Y": 0,
			"Filter.Transform.Position.Z": 0.00,

			"Filter.Transform.Rotation.X": i,
			"Filter.Transform.Rotation.Y": 0,
			"Filter.Transform.Rotation.Z": 0,

			// "Filter.Transform.Scale.X": 0,
			"Filter.Transform.Scale.Y": 110,
		}
		filterName := "transform"
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
	}
	<-ticker.C
}

func ChadBegin(c *obsws.Client, mutex *sync.Mutex) {
	ToggleSourceFilter(c, mutex, "922", "transform", true)

	start := 0.00
	end := 259.26
	// ticker := time.NewTicker(50 * time.Millisecond)

	for i := start; i < end; i += 0.03 {
		res := FetchFilterSettings(c, mutex, "922", "transform")
		fmt.Printf("res = %+v\n", res)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     1,
			"Filter.Transform.Position.Y": -0.12999999999999998,
			"Filter.Transform.Rotation.X": 58.65,
			"Filter.Transform.Rotation.Y": 3.03,
			"Filter.Transform.Rotation.Z": 0,
			"Filter.Transform.Scale.X":    111.11,
			"Filter.Transform.Scale.Y":    i,
		}
		sourceName := "922"
		filterName := "transform"
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
	}
	// <-ticker.C
}

func BlackGlasses(c *obsws.Client, mutex *sync.Mutex) {
	ToggleSource(c, mutex, "codin", "i_need_attention", true)
	ToggleSourceFilter(c, mutex, "i_need_attention", "transform", true)

	start := -50.00
	end := 64.61
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i < end; i += 0.01 {
		res := FetchFilterSettings(c, mutex, "i_need_attention", "transform")
		fmt.Printf("res = %+v\n", res)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": i,
			"Filter.Transform.Position.X":         -i,
			"Filter.Transform.Position.Y":         10,
		}
		sourceName := "i_need_attention"
		filterName := "transform"
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
	}
	<-ticker.C
}

func Zoom(c *obsws.Client, mutex *sync.Mutex, source string, level float64) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera":             1,
		"Filter.Transform.Camera.FieldOfView": level,
	}
	mutex.Lock()
	req := obsws.NewSetSourceFilterSettingsRequest(source, "transform", filterSettings)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	fmt.Printf("err = %#v\n", err)
	fmt.Printf("resp = %+v\n", resp.Status())
	mutex.Unlock()
}

func ReactionZoom(c *obsws.Client, mutex *sync.Mutex, source string) {
	ToggleSourceFilter(c, mutex, source, "zoom", true)

	start := 100.0
	end := 20.0
	ticker := time.NewTicker(2 * time.Second)

	for i := start; i > end; i -= 20 {
		Zoom(c, mutex, source, i)
		<-ticker.C
	}
}

func Rise(c *obsws.Client, mutex *sync.Mutex, sourceName string) {
	// ToggleSource(c, mutex, "codin", "i_need_attention", true)
	ToggleSourceFilter(c, mutex, sourceName, "transform", true)

	start := 175.0
	end := 0.0
	ticker := time.NewTicker(5 * time.Millisecond)

	for i := start; i > end; i -= 0.05 {
		// res := FetchFilterSettings(c, mutex, "i_need_attention", "transform")
		// fmt.Printf("res = %+v\n", res)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
			// "Filter.Transform.Camera":             1,
			// "Filter.Transform.Camera.FieldOfView": i,
			// "Filter.Transform.Position.X":         -i,
			// "Filter.Transform.Position.Y":         10,
		}
		filterName := "transform"
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
		<-ticker.C
	}
}

func RiseFromTheGrave(c *obsws.Client, mutex *sync.Mutex) {
	// ToggleSource(c, mutex, "codin", "i_need_attention", true)
	ToggleSourceFilter(c, mutex, "922", "transform", true)

	start := 175.0
	end := 0.0
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i > end; i -= 0.005 {
		// res := FetchFilterSettings(c, mutex, "i_need_attention", "transform")
		// fmt.Printf("res = %+v\n", res)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
			// "Filter.Transform.Camera":             1,
			// "Filter.Transform.Camera.FieldOfView": i,
			// "Filter.Transform.Position.X":         -i,
			// "Filter.Transform.Position.Y":         10,
		}
		sourceName := "922"
		filterName := "transform"
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
	}
	<-ticker.C
}

func Hide(c *obsws.Client, mutex *sync.Mutex) {
	// ToggleSource(c, mutex, "codin", "i_need_attention", true)
	ToggleSourceFilter(c, mutex, "922", "transform", true)

	start := 100.0
	end := 150.0
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i < end; i += 0.0005 {
		// res := FetchFilterSettings(c, mutex, "i_need_attention", "transform")
		// fmt.Printf("res = %+v\n", res)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
			// "Filter.Transform.Camera":             1,
			// "Filter.Transform.Camera.FieldOfView": i,
			// "Filter.Transform.Position.X":         -i,
			// "Filter.Transform.Position.Y":         10,
		}
		sourceName := "922"
		filterName := "transform"
		mutex.Lock()
		req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
		if err := req.Send(*c); err != nil {
			log.Print(err)
		}
		resp, err := req.Receive()
		mutex.Unlock()
		fmt.Printf("resp = %+v\n", resp.Status())
		fmt.Printf("err = %#v\n", err)
	}
	<-ticker.C
}

func SuperSaiyan(c *obsws.Client, mutex *sync.Mutex) {
	// go BigBrain(c, mutex)
	PulsatingHighlight(c, mutex, "922", 50, 60, true)
	RiseFromTheGrave(c, mutex)
}

// Request - An OBS request
// Ugh....I want to just use a Response
type Request interface {
	Send(obsws.Client) error
	Receive() (obsws.SetSceneItemPositionResponse, error)
	// How do we allow us to use this
	// Receive() (obsws.Response, error)
}

func processMoveSceneItem(
	c *obsws.Client,
	mutex *sync.Mutex,
	req Request,
) error {

	mutex.Lock()
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("resp = %+v\n", resp)
	return err
}
