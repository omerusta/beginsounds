package obs

import (
	"sync"
	"time"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

func zoomseal(c *obsws.Client, mutex *sync.Mutex) {
	toggleTransform(c, mutex, "922", true)
	toggleTransform(c, mutex, "Screen", true)
	toggleTransform(c, mutex, "Chat", true)
	ToggleSource(c, mutex, "codin", "sealspin", true)
	ZoomAndSpin(c, "Screen", 5, mutex)
	ZoomAndSpin(c, "Chat", 5, mutex)
	ZoomAndSpin(c, "922", 5, mutex)
	PulsatingHighlight(c, mutex, "sealspin", 50, 7, true)
	ZoomAndSpin(c, "sealspin", 4, mutex)
}
func beginboy(c *obsws.Client, mutex *sync.Mutex) {
	ToggleSource(c, mutex, "codin", "beginboy", true)
	ToggleSource(c, mutex, "codin", "beginboy", true)
	RiseSource(c, mutex, "codin", "beginboy", 750.0, 300.0, 0.0)
	ToggleSource(c, mutex, "codin", "artmattcardflat", true)
	ZoomFieldOfView(c, mutex, "artmattcardflat")
	ToggleSource(c, mutex, "codin", "artmattfloppycard", true)
	AppearFromThinAir(c, mutex, "artmattfloppycard")
	ToggleSource(c, mutex, "codin", "JesterCode", true)
	PulsatingHighlight(c, mutex, "JesterCode", 50, 7, true)
	<-time.NewTimer(time.Second * 5).C
	ReturnToNormie(c, mutex)
}
