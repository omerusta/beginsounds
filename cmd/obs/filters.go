package obs

import (
	"fmt"
	"log"
	"strconv"
	"sync"
	"time"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

// ToggleFilter is meant to take a Scene, a filter
// and you can toggle a filter off easily!
func ToggleFilter(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	filterName string,
	toggle bool,
) {

	mutex.Lock()
	req := obsws.NewSetSourceFilterVisibilityRequest(sceneName, filterName, toggle)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("Filter Toggled: %+v\n", resp)
}

func toggleTransform(c *obsws.Client, mutex *sync.Mutex, sceneName string, toggle bool) {
	mutex.Lock()
	req := obsws.NewSetSourceFilterVisibilityRequest(sceneName, "transform", toggle)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("resp = %+v\n", resp)
}

func FetchFilterSettings(c *obsws.Client, mutex *sync.Mutex, sourceName string, filterName string) map[string]interface{} {
	mutex.Lock()
	req := obsws.NewGetSourceFilterInfoRequest(sourceName, filterName)
	// 4.278255445e+09
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp1, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("FetchFilterSettings: %+v\n", resp1.Settings)
	return resp1.Settings
}

// Turn it on
func OutlineColor(
	c *obsws.Client,
	mutex *sync.Mutex,
	color float64,
	sourceName string,
) {

	filterName := "outline"
	filterSettings := map[string]interface{}{"Filter.SDFEffects.Outline.Color": color}
	mutex.Lock()
	req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	if err != nil {
		log.Print(err)
	}
	mutex.Unlock()
	fmt.Printf("resp = %+v\n", resp)
}

func PulsatingHighlight(
	c *obsws.Client,
	mutex *sync.Mutex,
	sourceName string,
	delay time.Duration,
	duration time.Duration,
	toggleOff bool,
) {

	timer := time.NewTimer(duration * time.Second)

	go func() {
		for {
			select {
			case <-timer.C:
				ReturnToNormie(c, mutex)
				if toggleOff {
					ToggleSource(c, mutex, "codin", sourceName, false)
				}
				return
			default:
				for _, colorFloat := range colors {
					color, err := strconv.ParseFloat(colorFloat, 64)
					if err != nil {
						fmt.Printf("err = %+v\n", err)
					}
					// This might be overkill
					OutlineColor(c, mutex, color, sourceName)
					time.Sleep(time.Millisecond * delay)
				}
			}
		}
	}()

}

func ToggleSourceFilter(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	filter string,
	toggle bool,
) {

	mutex.Lock()
	req := obsws.NewSetSourceFilterVisibilityRequest(sceneName, filter, toggle)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	_, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("Error Toggle Filter %s | %s: %v\n", sceneName, filter, err)
}
