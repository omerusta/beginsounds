package obs

import (
	"fmt"
	"log"
	"sync"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

func FetchSourceSettings(c *obsws.Client, mutex *sync.Mutex, sourceName string) {
	// specified source exists but is not of expected type
	sourceType := "image_source"
	// sourceType := "ffmpeg_source"
	req := obsws.NewGetSourceSettingsRequest(sourceName, sourceType)

	mutex.Lock()
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("resp = %+v\n", resp.SourceSettings)
	// fmt.Printf("resp = %+v\n", resp1.Settings)
	// return resp1.Settings
}

// FetchSources - Fetch a list of all the sources
func FetchSources(c *obsws.Client, mutex *sync.Mutex) {
	req := obsws.NewGetSourcesListRequest()
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("resp = %+v\n", resp.Sources)
}
