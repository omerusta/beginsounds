package obs

import (
	"sync"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

// ReturnToNormie This is meant to return all of BeginWorld
// 	back to a normal state
func ReturnToNormie(c *obsws.Client, mutex *sync.Mutex) {
	standardSources := []string{
		"Screen",
		"Chat",
		"922",
		"keyboard",
	}
	transformSources := []string{
		"beginworld",
		"sealspin",
		"puppy",
	}
	scenes := []string{
		"codin",
		"TopRight",
		"TopLeft",
		"BottomLeft",
		// "Giger",
		// "hottub",
	}
	toggleSources := []string{
		"922-left-bottom",
		"922-left-top",
		"922-right-top",
		"primeagen",
		"melkman",
		"teejpopup",
		"melkeyussr",
		"beginboy",
		"artmattcardflat",
		"artmattfloppycard",
		"JesterCode",
	}

	for _, source := range standardSources {
		ToggleSourceFilter(c, mutex, source, "outline", false)
		ToggleSourceFilter(c, mutex, source, "transform", false)
	}

	for _, source := range transformSources {
		ToggleSourceFilter(c, mutex, source, "outline", false)
	}

	for _, scene := range scenes {
		for _, source := range transformSources {
			ToggleSource(c, mutex, scene, source, false)
		}
		ToggleSource(c, mutex, "codin", "i_need_attention", false)
	}
	ToggleSourceFilter(c, mutex, "922", "zoom", false)
	ToggleSourceFilter(c, mutex, "Alerts", "transform", false)
	ToggleSourceFilter(c, mutex, "Alerts", "outline", false)

	for _, source := range toggleSources {
		ToggleSource(c, mutex, "codin", source, false)
	}

	// These is ruining too much fun
	// ResetTransform(c, "Alerts", mutex)
	// ResetTransform(c, "922", mutex)
	// ResetTransform(c, "keyboard", mutex)
	// ResetTransform(c, "Chat", mutex)
}
