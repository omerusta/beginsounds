package obs

import (
	"fmt"
	"log"
	"math/rand"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	obsws "github.com/christopher-dG/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

// TrollBegin is the main loop processing user messages
//   to "troll" Begin and trigger various OBS effects
//   with Websockets
func TrollBegin(db *gorm.DB, commands <-chan chat.ChatMessage) <-chan string {
	results := make(chan string, 1000)

	go func() {
		c := obsws.Client{Host: "localhost", Port: 4444}
		var mutex = &sync.Mutex{}
		if err := c.Connect(); err != nil {
			log.Print(err)
		}
		defer c.Disconnect()
		defer close(results)

		configFile, _ := filepath.Abs("config/obs_commands.json")
		config := parseConfig(configFile)
		fmt.Printf("config = %+v\n", config)

	Loop:
		for msg := range commands {
			// Do we need to call this on every loop?
			rand.Seed(time.Now().UnixNano())

			var p *player.Player
			if msg.PlayerID != nil {
				p = player.FindByID(db, *msg.PlayerID)
			} else if msg.PlayerName != "" {
				p = player.Find(db, msg.PlayerName)
			}

			// If we don't have a user then we don't know whether
			// you are allowed to Troll Begin, so we just continue
			if p == nil {
				fmt.Printf("msg = %+v\n", msg)
				continue Loop
			}
			if p.ID == 0 {
				continue Loop
			}

			mana := p.Mana

			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			// We allow everyone to return Begin to Normie
			if cmd == "!normie" {
				// Include more values here
				ReturnToNormie(&c, mutex)
				ResetTransform(&c, "Alerts", mutex)
				ResetTransform(&c, "keyboard", mutex)
				ResetTransform(&c, "Chat", mutex)
				continue Loop
			}

			// User Specific ===========================

			// This is why we are spazzing!
			// This should also be a function
			if cmd == "!teej3" {
				ToggleSource(&c, mutex, "codin", "teejpopup", true)
				max := 500
				min := -100
				startX := 0.0
				endX := 275.0
				y := float64(rand.Intn(max-min) + min)
				// Should we pass these in like this???
				go func(
					client *obsws.Client,
					m *sync.Mutex,
					start float64,
					end float64,
					yValue float64,
				) {
					// This min max is great for side teej
					SlideSource(client, m, "codin", "teejpopup", start, end, yValue)
					<-time.NewTimer(time.Second * 1).C
					ReverseSlideSource(client, m, "codin", "teejpopup", end, start, yValue, -1)
				}(&c, mutex, startX, endX, y)
			}

			if cmd == "!bg" {
				// if p.Name == "thealtf4stream" && cmd == "!bg" {
				BlackGlasses(&c, mutex)
				ReturnToNormie(&c, mutex)
			}

			// need to do user matching earlier

			// Check if the Jester is current player
			// or if we are in Chaos Mode
			jester := stream_jester.CurrentJester(db)
			if !jester.ChaosMode && !msg.Streamgod {
				if jester.Secret == cmd[1:] && jester.PlayerID == nil {
					db.Model(&jester).Update("player_id", msg.PlayerID)
					results <- fmt.Sprintf("New Jester! @%s", msg.PlayerName)
					waitForJester := time.NewTimer(1 * time.Second)
					<-waitForJester.C
					ChangeScene(&c, mutex, "jester_scene")
					continue Loop
				}
				if jester == nil || jester.PlayerID == nil || *jester.PlayerID != p.ID {
					continue Loop
				}
			}

			fmt.Println("\n\t~~~~~ Past the Jester!!!")

			if cmd == "!passthejester" {
				waitForJester := time.NewTimer(500 * time.Millisecond)
				<-waitForJester.C
				ChangeScene(&c, mutex, "jester_scene")
				continue Loop
			}

			// ---------------------------------------------------------------------------------

			switch cmd {

			case "!createsource":
				fmt.Printf("\t\t ~~~~~~ msgBreakdown = %+v\n", msgBreakdown)

				if len(msgBreakdown) > 1 {
					imageName := msgBreakdown[1]
					fmt.Printf("\t -- We are going to try and Create a new image: %s", imageName)

					mr, err := media_request.FindByName(db, imageName)
					if err != nil {
						fmt.Printf("err = %+v\n", err)
						continue Loop
					}

					filename := fmt.Sprintf("/home/begin/stream/Stream/ViewerImages/%s.png", mr.Name)
					fmt.Printf("filename = %+v\n", filename)
					fmt.Printf("mr.Name = %+v\n", mr.Name)

					CreateImageSource(&c, mutex, "codin", mr.Name, filename)
					db.Model(&mr).Update("deleted_at", time.Now())
					// we need to look up the media_request here by name
				}

			// ============= //
			// Toggle Scenes //
			// ============= //
			case "!hateflow":
				ChangeScene(&c, mutex, "emperor")
			case "!h":
				ChangeScene(&c, mutex, "BottomLeft")
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!j":
				ChangeScene(&c, mutex, "TopLeft")
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!k":
				ChangeScene(&c, mutex, "TopRight")
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!l":
				ChangeScene(&c, mutex, "codin")
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))

			// ============== //
			// Toggle Sources //
			// ============== //
			case "!catjam":
				ToggleSource(&c, mutex, "codin", "shia", true)
			case "!lbbegin":
				ToggleSource(&c, mutex, "codin", "922-left-bottom", true)
			case "!ltbegin":
				ToggleSource(&c, mutex, "codin", "922-left-top", true)
			case "!rtbegin":
				ToggleSource(&c, mutex, "codin", "922-right-top", true)

			// =================== //
			// Normalizing Effects //
			// =================== //
			case "!normie_transform":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ToggleFilter(&c, mutex, "922", "normie_transform", true)
			case "!norm":
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 1 {
					source := parts[1]
					ToggleSourceFilter(&c, mutex, source, "transform", true)
					ToggleFilter(&c, mutex, source, "normie_transform", true)
				}

			// ==================== //
			// Begin Camera Effects //
			// ==================== //
			case "!rise":
				RiseFromTheGrave(&c, mutex)
			case "!pulse":
				sourceName := "922"
				ToggleSourceFilter(&c, mutex, sourceName, "outline", true)
				PulsatingHighlight(&c, mutex, sourceName, 50, 6, false)
			case "!reaction":
				ReactionZoom(&c, mutex, "922")
			case "!bigbrain":
				go BigBrain(&c, mutex, "922")
			case "!chad":
				go ChadBegin(&c, mutex)
			case "!zoombegin":
				toggleTransform(&c, mutex, "922", true)
				toggleTransform(&c, mutex, "Screen", true)
				TheZoom(&c, "922", 50, mutex)
				TheZoom(&c, "Screen", 50, mutex)
				TheZoom(&c, "Chat", 50, mutex)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!zoombegin2":
				// toggleTransform(&c, mutex, "Chat", true)
				// ToggleSource(&c, mutex, "codin", "sealspin", true)
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ToggleSourceFilter(&c, mutex, "922", "outline", true)
				// False means don't turn this source off!
				go ZoomAndSpin2(&c, "922", 10, mutex)
				go PulsatingHighlight(&c, mutex, "922", 50, 10, false)
			case "!spazzbegin":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ToggleFilter(&c, mutex, "922", "spazzbegin", true)
			case "!rollbegin":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ToggleFilter(&c, mutex, "922", "rollbegin", true)
			case "!flipbegin":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ToggleFilter(&c, mutex, "922", "flipbegin", true)
			case "!bigbrainbegin":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ToggleFilter(&c, mutex, "922", "bigbrainbegin", true)
			case "!zoombegin3":
				// toggleTransform(&c, mutex, "Chat", true)
				// ToggleSource(&c, mutex, "codin", "sealspin", true)
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ToggleSourceFilter(&c, mutex, "922", "outline", true)
				// False means don't turn this source off!
				go BigBrain(&c, mutex, "922")
				go PulsatingHighlight(&c, mutex, "922", 50, 15, false)
				go ZoomAndSpin3(&c, "922", 15, mutex)
			case "!zoom2":
				toggleTransform(&c, mutex, "922", true)
				toggleTransform(&c, mutex, "Screen", true)
				toggleTransform(&c, mutex, "Chat", true)
				ToggleSource(&c, mutex, "codin", "sealspin", true)
				go ZoomAndSpin2(&c, "Screen", 10, mutex)
				go ZoomAndSpin2(&c, "Chat", 10, mutex)
				go ZoomAndSpin(&c, "922", 10, mutex)
				go PulsatingHighlight(&c, mutex, "sealspin", 50, 7, true)
				go ZoomAndSpin(&c, "sealspin", 10, mutex)
			case "!zoom3":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				level := 90.0
				// Can we add pulsating here
				for i := level; i < 300; i += 0.003 {
					fmt.Printf("i = %+v\n", i)
					Zoom(&c, mutex, "922", i)
				}
				go Zoom(&c, mutex, "922", 90.0)
				// toggleSourceFilter(&c, mutex, "922", "transform", false)
			case "!tallbegin":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				TallBegin(&c, "922", 30, mutex)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!widebegin":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				go func() {
					WideBegin(&c, "922", 7, mutex)
					player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
				}()
			case "!zoom4":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				level := 90.0
				// Can we add pulsating here
				for i := level; i > -300; i -= 0.003 {
					fmt.Printf("i = %+v\n", i)
					Zoom(&c, mutex, "922", i)
				}
				go Zoom(&c, mutex, "922", 90.0)
				// toggleSourceFilter(&c, mutex, "922", "transform", false)
			case "!highlight":
				ToggleSourceFilter(&c, mutex, "922", "outline", true)
				randomIndex := rand.Intn(len(colors))
				color, _ := strconv.ParseFloat(colors[randomIndex], 64)
				OutlineColor(&c, mutex, color, "922")
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!color_green":
				color, _ := strconv.ParseFloat(GREEN, 64)
				OutlineColor(&c, mutex, color, "922")
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!colorbrown":
				color, err := strconv.ParseFloat(BROWN, 64)
				if err != nil {
					fmt.Printf("err = %+v\n", err)
				}
				OutlineColor(&c, mutex, color, "922")
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!hidebegin":
				Hide(&c, mutex)
			case "!intro":
				ChangeScene(&c, mutex, "startin")
				ToggleSourceFilter(&c, mutex, "keyboard", "outline", true)
				ToggleSource(&c, mutex, "startin", "keyboard", true)
				toggleTransform(&c, mutex, "keyboard", true)
				PulsatingHighlight(&c, mutex, "keyboard", 50, 30, false)
				Rise(&c, mutex, "keyboard")
				ZoomAndSpin2(&c, "keyboard", 8, mutex)
				// could we wait and spin
				// Change in to intro source
				// Pulsate on Keyboard
				// Start Rise

			// ============== //
			// Viewer Effects //
			// ============== //
			case "!obieru":
				sourceName := "obieru"
				ToggleSource(&c, mutex, "codin", sourceName, true)
				max := 1000
				min := 200
				x := float64(rand.Intn(max-min) + min)
				startY := 600.0
				endY := -500.0
				RiseSource(&c, mutex, "codin", sourceName, startY, endY, x)
				ToggleSource(&c, mutex, "codin", sourceName, false)
			case "!melkman2":
				ToggleSource(&c, mutex, "codin", "melkman", true)
				SetSourcePosition(&c, mutex, "codin", "melkman", 0, 0)
				ZoomAndSpin(&c, "melkman", 6, mutex)
			case "!melkman":
				ToggleSource(&c, mutex, "codin", "melkman", true)
				max := 1000
				min := 200
				x := float64(rand.Intn(max-min) + min)

				startY := 600.0
				endY := -500.0
				RiseSource(&c, mutex, "codin", "melkman", startY, endY, x)
				ToggleSource(&c, mutex, "codin", "melkman", false)
			case "!primeagen":
				ToggleSource(&c, mutex, "codin", "primeagen", true)
				SlideSource(&c, mutex, "codin", "primeagen", 0.0, 1165.0, 631.0)
			case "!roxkstar1":
				ToggleSource(&c, mutex, "codin", "roxkstar", true)
				BigBrain(&c, mutex, "roxkstar")
				ToggleSource(&c, mutex, "codin", "roxkstar", false)
			case "!teej1":
				SlideInFromSide(&c, mutex, "codin", "teejpopup")
			case "!teej":
				teejs := []string{
					"teejpopup",
					"teejlaugh",
				}
				f := randomTransformFunc()
				teej := teejs[rand.Intn(len(teejs))]
				f(&c, mutex, "codin", teej)
			case "!teej2":
				floatSourceThroughScene(&c, mutex, "codin", "teejlaugh")
			case "!linus":
				f := randomTransformFunc()
				f(&c, mutex, "codin", "linusjobs")
			case "!rms2":
				f := randomTransformFunc()
				f(&c, mutex, "codin", "rms2")

				// we can easily pass any function to this

			// ================ //
			// Generic Commands //
			// ================ //
			case "!zoom":
				// We need to look for sources
				// to know whether is empty
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 1 {
					source := parts[1]
					toggleTransform(&c, mutex, source, true)
					go ZoomAndSpin2(&c, source, 10, mutex)
					// go ZoomAndSpin(&c, source, 10, mutex)
					// go PulsatingHighlight(&c, mutex, source, 50, 7, true)
				}
			case "!spin":
				parts := strings.Split(msg.Message, " ")
				// We need to look for sources
				// to know whether is empty
				if len(parts) > 1 {
					source := parts[1]
					toggleTransform(&c, mutex, source, true)
					go ZoomAndSpin(&c, source, 10, mutex)
				}
			case "!scale":
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 2 {
					source := parts[1]
					scale, _ := strconv.ParseFloat(parts[2], 64)
					ScaleDownSource(&c, mutex, "codin", source, scale)
				}
			case "!rotate":
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 2 {
					source := parts[1]
					rotation, _ := strconv.ParseFloat(parts[2], 64)
					RotateSource(&c, mutex, "codin", source, rotation)
				}
			case "!random":
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 1 {
					f := randomTransformFunc()
					source := parts[1]
					f(&c, mutex, "codin", source)
				}
			case "!show":
				parts := strings.Split(msg.Message, " ")

				if len(parts) > 1 {
					source := parts[1]
					// Saw I wanted to change true to false
					// thought there was a plugin for flipping
					// went through tpope plugins
					// found a new plugin I need to use
					// came back and said nice, fixed with updating false
					ToggleSource(&c, mutex, "codin", source, true)
				}
			case "!hide":
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 1 {
					source := parts[1]
					ToggleSource(&c, mutex, "codin", source, false)
				}
			case "!center":
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 1 {
					source := parts[1]
					CenterSource(&c, mutex, "codin", source)
				}
			case "!slide":
				parts := strings.Split(msg.Message, " ")
				if len(parts) > 1 {
					source := parts[1]
					yPosition := 0.0
					flatImages := []string{"primeagen", "santa"}
					for _, i := range flatImages {
						if source == i {
							yPosition = 631.0
						}
					}
					// We need to figure out the image size
					/// but for now lets be dumb
					SlideSource(&c, mutex, "codin", source, 0.0, 1165.0, yPosition)
				}

			// ==================== //
			// Toggle and Transform //
			// ==================== //
			case "!alerts":
				sourceName := "Alerts"
				ToggleSourceFilter(&c, mutex, sourceName, "transform", true)
				ZoomAndSpin(&c, sourceName, 5, mutex)
			// Theory: We aren't getting
			// effects lower in this list
			case "!beginworld":
				ToggleSource(&c, mutex, "codin", "beginworld", true)
				toggleTransform(&c, mutex, "beginworld", true)
				ZoomAndSpin(&c, "beginworld", 8, mutex)
			case "!seal":
				ToggleSource(&c, mutex, "codin", "sealspin", true)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!puppy":
				ToggleSource(&c, mutex, "codin", "puppy", true)
				var duration time.Duration
				duration = 30
				DelaySpin(&c, mutex, "codin", "puppy", 2, duration)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!zoomseal":
				zoomseal(&c, mutex)
			case "!pulseseal":
				ToggleSource(&c, mutex, "codin", "sealspin", true)
				ToggleSourceFilter(&c, mutex, "sealspin", "transform", true)
				ToggleSourceFilter(&c, mutex, "sealspin", "outline", true)
				PulsatingHighlight(&c, mutex, "sealspin", 50, 7, true)
				ZoomAndSpin(&c, "sealspin", 5, mutex)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!otherspin":
				ToggleSourceFilter(&c, mutex, "922", "transform", true)
				ZoomAndSpin(&c, "922", 7, mutex)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!spinseal":
				ToggleSource(&c, mutex, "codin", "sealspin", true)
				ToggleSourceFilter(&c, mutex, "sealspin", "transform", true)
				ZoomAndSpin(&c, "922", 5, mutex)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!beginboy":
				beginboy(&c, mutex)

			// =============== //
			// Helper Settings //
			// =============== //
			case "!filtersettings":
				// res := FetchFilterSettings(&c, mutex, "922", "transform")
				res := FetchFilterSettings(&c, mutex, "922", "transform")
				fmt.Printf("\tSettings = %+v\n", res)
				results <- fmt.Sprintf("%v", res)
				player.UpdateMana(db, *msg.PlayerID, uint(mana-10))
			case "!settings":
				FetchSourceSettings(&c, mutex, "sealspin")
			}

		}

	}()

	obsws.SetReceiveTimeout(time.Second * 2)
	return results
}

type sourceTransform = func(*obsws.Client, *sync.Mutex, string, string)

var funcOpts = []sourceTransform{
	SlideInFromSide,
	floatSourceThroughScene,
}

func randomTransformFunc() sourceTransform {
	randInt := rand.Intn(len(funcOpts))
	return funcOpts[randInt]
}
