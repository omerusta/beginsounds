package obs

import (
	"fmt"
	"path/filepath"
	"testing"
)

func TestObsConfig(t *testing.T) {
	// db := database.CreateDBConn("beginsounds4")
	// c := obsws.Client{Host: "localhost", Port: 4444}
	// var mutex = &sync.Mutex{}
	// if err := c.Connect(); err != nil {
	// 	log.Fatal(err)
	// }
	// defer c.Disconnect()
	configFile, _ := filepath.Abs("../../config/obs_commands.json")
	config := parseConfig(configFile)
	fmt.Printf("config = %+v\n", config)
	// We can do stuff
	// t.Errorf("HAVING FUN")
}

func TestNiceTestObs(t *testing.T) {
	return
	// db := database.CreateDBConn("beginsounds4")
	// c := obsws.Client{Host: "localhost", Port: 4444}
	// var mutex = &sync.Mutex{}
	// if err := c.Connect(); err != nil {
	// 	log.Fatal(err)
	// }
	// defer c.Disconnect()
	// res := FetchFilterSettings(&c, mutex, "922", "transform")
	// fmt.Printf("Hello34: %+v\n", res)
	// ToggleSource(&c, mutex, "codin", "primeagen", true)
	// SlideSource(&c, mutex, "codin", "primeagen", 0.0, 1165.0, 631.0)

	// ToggleSource(&c, mutex, "codin", "teejpopup", true)
	// rand.Seed(time.Now().UnixNano())

	// This min max is great for side teej
	// max := 500
	// min := -100
	// startX := 0.0
	// endX := 275.0
	// y := float64(rand.Intn(max-min) + min)

	// max := 1800
	// min := 200
	// x := float64(rand.Intn(max-min) + min)

	// startY := 600.0
	// endY := -600.0
	// x := 200
	// RiseSource(&c, mutex, "codin", "teejlaugh", startY, endY, x)
	// SlideSource(&c, mutex, "codin", "teejpopup", startX, endX, y)
	// <-time.NewTimer(time.Second * 1).C
	// ReverseSlideSource(&c, mutex, "codin", "teejpopup", endX, startX, y, -1)
	// SlideSource(&c, mutex, "codin", "teejpopup", 0.0, 300, 400)
	return

	// filterName := "transform"
	// sourceName := "922"
	// filterSettings := map[string]interface{}{
	// 	"Filter.Transform.Camera.FieldOfView": 90.00,
	// 	"Filter.Transform.Position.Y":         0.00,
	// 	"Filter.Transform.Position.Z":         0.00,
	// 	"Filter.Transform.Rotation.X":         0.00,
	// 	"Filter.Transform.Rotation.Y":         0.00,
	// 	"Filter.Transform.Rotation.Z":         0.00,
	// 	"Filter.Transform.Scale.X":            90.00,
	// 	"Filter.Transform.Scale.Y":            90.00,
	// 	"Filter.Transform.Shear.X":            0.0,
	// 	"Filter.Transform.Shear.Y":            0.0,
	// }
	// req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
	// if err := req.Send(c); err != nil {
	// 	log.Fatal(err)
	// }
	// resp, err := req.Receive()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Printf("resp2 = %+v\n", resp)

	// x := 100.0
	// mutex.Lock()
	// req := obsws.NewGetSceneItemPositionRequest("codin", "primeagen")
	// req := obsws.NewGetSceneItemPropertiesRequest("codin", "primeagen")
	// if err := req.Send(c); err != nil {
	// 	log.Fatal(err)
	// }
	// resp, err := req.Receive()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Printf("resp3 = %+v\n", resp)
	// mutex.Unlock()

	// ToggleSource(&c, mutex, "codin", "922-left-bottom", false)
	// ToggleSource(&c, mutex, "codin", "922-left-top", false)
	// ToggleSource(&c, mutex, "codin", "922-right-top", false)

	// ToggleFilter(&c, mutex, "922", "normie_transform", true)
	// return

	// BigBrain(&c, mutex)
	// ChadBegin(&c, mutex)

	// ToggleFilter(&c, mutex, "922", "normie_transform", false)
	// ToggleFilter(&c, mutex, "922", "flipbegin", true)
	// ToggleFilter(&c, mutex, "922", "spazzbegin", true)
	// ToggleFilter(&c, mutex, "922", "rollbegin", false)

	// BigBrain(&c, mutex)
	// ToggleFilter(&c, mutex, "922", "spazzbegin", true)
	// ToggleFilter(&c, mutex, "922", "flipbegin", true)
	// ToggleSource(&c, mutex, "codin", "922-left-bottom", true)
	// <-time.NewTimer(1 * time.Second).C
	// ToggleSource(&c, mutex, "codin", "922-left-top", true)
	// <-time.NewTimer(1 * time.Second).C
	// ToggleSource(&c, mutex, "codin", "922-right-top", true)
	// ToggleFilter(&c, mutex, "922", "spazzbegin", true)

	// ChadBegin(&c, mutex)
	// ToggleFilter(&c, mutex, "922", "normie_transform", true)
	// ToggleFilter(&c, mutex, "922", "normie_transform", false)

	// BigBrain(&c, mutex, "922")
	// ToggleFilter(&c, mutex, "922", "normie_transform", true)
	// ToggleFilter(&c, mutex, "922", "normie_transform", false)

	// ToggleFilter(&c, mutex, "922", "flipbegin", true)
	// ToggleFilter(&c, mutex, "922", "normie_transform", true)
	// ToggleFilter(&c, mutex, "922", "normie_transform", false)

	// ToggleFilter(&c, mutex, "922", "spazzbegin", true)
	// ToggleFilter(&c, mutex, "922", "normie_transform", true)
	// ToggleFilter(&c, mutex, "922", "normie_transform", false)

	// ToggleFilter(&c, mutex, "922", "rollbegin", true)
	// ToggleFilter(&c, mutex, "922", "normie_transform", true)

	// These need to return to normal
	// srses = map[Commit:a0ce2960 Filter.Transform.Camera:1 Filter.Transform.Camera.FieldOfView:90 Filter.Transform.Rotation.X:232.39999999999867 Filter.Transform.Rotation.Y:-232.39999999999867 Filter.Transform.Rotation.Z:332 Filter.Transform.Scale.X:232 Filter.Transform.Scale.Y:232 Version:3.8654705692e+10]

	// ReturnToNormie(&c, "", mutex)
	// fmt.Println("try5")
	// SuperSaiyan(&c, mutex)
	// Filter.Transform.Position.Y:100

	// stream_command.CreateFromName(db, "womf")

	// ReactionZoom(&c, mutex, "922")

	// This is return to default settings
	// Zoom(&c, mutex, "922", 100)

	// ChadBegin(&c, mutex)

	// BigBrain(&c, mutex)

	// BlackGlasses(&c, mutex)
	// Hide(&c, mutex)
	// RiseFromTheGrave(&c, mutex)
}
