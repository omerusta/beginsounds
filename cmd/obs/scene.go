package obs

import (
	"log"
	"sync"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

// ChangeScene simple call to change the OBS Scene
func ChangeScene(c *obsws.Client, mutex *sync.Mutex, sceneName string) {
	mutex.Lock()
	req := obsws.NewSetCurrentSceneRequest(sceneName)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	log.Println("Scene Changed: ", resp)
}
