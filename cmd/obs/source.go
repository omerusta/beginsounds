package obs

import (
	"fmt"
	"log"
	"sync"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

// ToggleSource - allows toggling a source on or off
func ToggleSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	toggle bool,
) {
	mutex.Lock()
	req := obsws.NewSetSceneItemRenderRequest(
		sceneName, sourceName, toggle,
	)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("Toggle Source: %+v\n", resp.Status())
}

// CreateMediaSource creates a ffmpeg source in OBS
func CreateMediaSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	filename string,
) error {
	sourceKind := "ffmpeg_source"
	return createSource(c, mutex, sceneName, sourceName, filename, sourceKind)
}

// CreateImageSource create a new image on the codin source
func CreateImageSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	filename string,
) error {
	sourceKind := "image_source"
	return createSource(c, mutex, sceneName, sourceName, filename, sourceKind)
}

// TODO: This should look to see if a source already exists
func createSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	filename string,
	sourceKind string,
) error {
	settings := map[string]interface{}{
		"file": filename,
	}
	req := obsws.NewCreateSourceRequest(
		sourceName,
		sourceKind,
		sceneName,
		settings,
		true,
	)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("resp = %+v\n", resp)
	return nil
}

func ScaleDownSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	scale float64,
) error {
	req := obsws.NewSetSceneItemTransformRequest(
		sceneName,
		sourceName,
		scale,
		scale,
		0.0, // Rotation
	)
	mutex.Lock()
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}

	resp, err := req.Receive()
	fmt.Printf("resp = %+v\n", resp)

	mutex.Unlock()
	return err
}

func RotateSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
	rotation float64,
) error {

	req := obsws.NewGetSceneItemPropertiesRequest(
		sceneName,
		nil,
		sourceName,
		0,
	)
	mutex.Lock()
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	ogScene, err := req.Receive()
	mutex.Unlock()

	fmt.Printf("\togScene.Rotation = %+v\n", ogScene.Rotation)
	fmt.Printf("\tNew Rotation: %f", rotation)

	// In OBS -> I say transform Flip Source Vertical
	// Once I do that 0.0 returns Stallman to upside down
	req1 := obsws.NewSetSceneItemPropertiesRequest(
		sceneName,
		sourceName,
		sourceName, // itemName string,
		0,          // ID
		ogScene.PositionX,
		ogScene.PositionY,
		ogScene.PositionAlignment,
		// 0.0,
		float64(rotation),
		ogScene.ScaleX,
		ogScene.ScaleY,
		ogScene.CropTop,
		ogScene.CropBottom,
		ogScene.CropLeft,
		ogScene.CropRight,
		true,  // visible
		false, // locked
		ogScene.BoundsType,
		ogScene.BoundsAlignment,
		ogScene.BoundsX,
		ogScene.BoundsY)

	mutex.Lock()
	if err := req1.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req1.Receive()
	fmt.Printf("resp = %+v\n", resp)
	mutex.Unlock()
	return err
}

func CenterSource(
	c *obsws.Client,
	mutex *sync.Mutex,
	sceneName string,
	sourceName string,
) error {

	req := obsws.NewGetSceneItemPropertiesRequest(
		sceneName,
		nil,
		sourceName,
		0,
	)
	mutex.Lock()
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	ogScene, err := req.Receive()
	mutex.Unlock()

	// In OBS -> I say transform Flip Source Vertical
	// Once I do that 0.0 returns Stallman to upside down
	req1 := obsws.NewSetSceneItemPropertiesRequest(
		sceneName,
		sourceName,
		sourceName, // itemName string,
		0,          // ID
		0.0,        // ogScene.PositionX,
		0.0,        // ogScene.PositionY,
		ogScene.PositionAlignment,
		// 0.0,
		ogScene.Rotation, // Maybe I should return to default
		ogScene.ScaleX,
		ogScene.ScaleY,
		ogScene.CropTop,
		ogScene.CropBottom,
		ogScene.CropLeft,
		ogScene.CropRight,
		true,  // visible
		false, // locked
		ogScene.BoundsType,
		ogScene.BoundsAlignment,
		ogScene.BoundsX,
		ogScene.BoundsY)

	mutex.Lock()
	if err := req1.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req1.Receive()
	fmt.Printf("resp = %+v\n", resp)
	mutex.Unlock()
	return err
}
