package obs

import (
	"fmt"
	"log"
	"sync"

	obsws "github.com/christopher-dG/go-obs-websocket"
)

func ResetTransform(c *obsws.Client, sourceName string, mutex *sync.Mutex) {
	filterName := "transform"

	filterSettings := map[string]interface{}{
		// "Filter.Transform.Scale.X":    x,
		// "Filter.Transform.Scale.Y":    y,
		// "Filter.Transform.Rotation.X": x,
		// "Filter.Transform.Rotation.Y": y,
		// "Filter.Transform.Rotation.Z": z,

		"Filter.Transform.Camera.FieldOfView": 90.00,
		"Filter.Transform.Position.Y":         0.00,
		"Filter.Transform.Position.Z":         0.00,
		"Filter.Transform.Rotation.X":         0.00,
		"Filter.Transform.Rotation.Y":         0.00,
		"Filter.Transform.Rotation.Z":         0.00,
		"Filter.Transform.Scale.X":            90.00,
		"Filter.Transform.Scale.Y":            90.00,
		"Filter.Transform.Shear.X":            0.0,
		"Filter.Transform.Shear.Y":            0.0,
	}

	mutex.Lock()
	req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, err := req.Receive()
	mutex.Unlock()
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("resp = %+v\n", resp)
}
