package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
	"gorm.io/gorm"
)

func PlayersJsonToPG(db *gorm.DB, filename string) []player.Player {
	var jsonplayers map[string]map[string]player.Player
	var players []player.Player
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(fmt.Sprintf("%v Error Reading in %s", err, filename))
	}
	err = json.Unmarshal(data, &jsonplayers)
	for _, u := range jsonplayers["users"] {
		players = append(players, u)
		db.Save(&u)
	}
	return players
}

func CommandsJsonToPG(db *gorm.DB,
	jsonCommands map[string]map[string]stream_command.StreamCommand) []stream_command.StreamCommand {
	var commands []stream_command.StreamCommand

	for _, c := range jsonCommands["commands"] {
		commands = append(commands, c)
		db.Table("stream_commands").Create(&c)

		for _, username := range c.Users {
			if username != "" {
				p := player.Find(db, username)
				player.AllowAccess(db, p.ID, c.ID)
			} else {
				fmt.Printf("sc.Name = %+v\n", c.Name)
			}
		}

	}

	return commands
}

func worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Println("worker", id, "started  job", j)
		time.Sleep(time.Second)
		fmt.Println("worker", id, "finished job", j)
		results <- j * 2
	}
}

func main2() {
	const numJobs = 5
	jobs := make(chan int, numJobs)
	results := make(chan int, numJobs)
	for w := 1; w <= 3; w++ {
		go worker(w, jobs, results)
	}
	for j := 1; j <= numJobs; j++ {
		jobs <- j
	}
	close(jobs)
	for a := 1; a <= numJobs; a++ {
		<-results
	}
}

func main() {
	dbName := flag.String("db", "test_beginsounds", "The name of the database to run migrations against")
	flag.Parse()
	db := database.CreateDBConn(*dbName)
	test_support.ClearDb(db)

	// TODO: Remove this hardcoded paths
	filename := "/home/begin/code/chat_thief/db/users.json"
	PlayersJsonToPG(db, filename)

	filename2 := "/home/begin/code/chat_thief/db/commands.json"
	data, err := ioutil.ReadFile(filename2)
	if err != nil {
		log.Fatal("Error Reading in Commands JSON DB")
	}
	var jsonCommands map[string]map[string]stream_command.StreamCommand
	err = json.Unmarshal(data, &jsonCommands)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	CommandsJsonToPG(db, jsonCommands)
}
