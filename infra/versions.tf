terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }
  }
  required_version = ">= 0.13"
}

variable "linode_token" {
  description = "API token to allow Terraform to create resources in our Linode Account"
}
