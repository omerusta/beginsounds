package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/beginbot/beginsounds/cmd/obs"
	"gitlab.com/beginbot/beginsounds/notifs"
	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat_saver"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme_router"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/criminal_activities"
	"gitlab.com/beginbot/beginsounds/pkg/cube_bet_router"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/economy_router"
	"gitlab.com/beginbot/beginsounds/pkg/filter"
	"gitlab.com/beginbot/beginsounds/pkg/hand_of_the_market"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/media_request_processor"
	"gitlab.com/beginbot/beginsounds/pkg/media_router"
	"gitlab.com/beginbot/beginsounds/pkg/notification_site"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/relationship_manager"
	"gitlab.com/beginbot/beginsounds/pkg/reporter"
	"gitlab.com/beginbot/beginsounds/pkg/router"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_processor"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_router"
	"gitlab.com/beginbot/beginsounds/pkg/streamgod_router"
	"gitlab.com/beginbot/beginsounds/pkg/tee"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
)

func main() {
	db := database.CreateDBConn("beginsounds4")
	// How do we clone the database easily?? pgdumb and pgrestore

	twitchChannel := flag.String("channel", "beginbot", "The main IRC Channel to connect to")
	isMarketOpen := flag.Bool("market", false, "If the market is open")
	connectToOBS := flag.Bool("obs", false, "Whether to connect to OBS")
	enableWebsocketNotifs := flag.Bool("websocket",
		false,
		"Whether to Send Websocket Notifications",
	)

	flag.Parse()

	ctx := context.Background()
	themes := colorscheme.GatherColors()

	fmt.Printf("Main Bot Running on Twitch Channel: %#v\n", *twitchChannel)
	c := config.NewIrcConfig(*twitchChannel)

	// We will need to launch another Go Routine to read
	// off from beginbotbot channel
	// and only respond to messages from Streamlords and Streamgods
	botChannelConfig := config.NewIrcConfig("beginbotbot")
	backAlleyMsgs := irc.ReadIrc(ctx, botChannelConfig.Conn)
	backAlleyCmds := router.FilterChatMsgs(ctx, db, &botChannelConfig, backAlleyMsgs)

	// Launch a Go Routine to collect messages from IRC
	messages := irc.ReadIrc(ctx, c.Conn)

	// Filter out only the User Chat messages from IRC
	// chatMsgs := router.FilterChatMsgs(ctx, db, &botChannelConfig, messages)
	beginbotMsgs := router.FilterChatMsgs(ctx, db, &c, messages)

	chatMsgs := tee.ChatFanIn(beginbotMsgs, backAlleyCmds)

	// Duplicate the User Messages, one to save one for further processing
	//  Fan In here the beginbot and beginbotbot messages
	chatMsgs1, chatMsgs2 := tee.DuplicateChatMessages(ctx, chatMsgs)

	var botResponses = []<-chan string{}
	var botbotResponses = []<-chan string{}
	var audioRequests = []<-chan audio_request.AudioRequest{}

	// Save the User Chat Messages
	// If the User hasn't chatted today, then a Theme Request
	// Will also be played
	//
	//
	// So how should we add OBS themes on theme requests
	// can we return them here
	themeRequests, obsThemes := chat_saver.SaveChat(ctx, db, chatMsgs1)
	audioRequests = append(audioRequests, themeRequests)

	// This filters out just attempted user commands
	userCommands, botResponses1 := filter.RouteUserCommands(ctx, db, chatMsgs2)
	botResponses = append(botResponses, botResponses1)

	// We should be able to pass in an number to return
	uc1, uc2, uc3, uc4, uc5, uc6, uc7, uc8, uc9, uc10, uc11, uc12, uc13, uc14, uc15 := tee.FanOut(
		ctx,
		userCommands,
	)

	adminResults2, botResponses18 := pack.Route(ctx, db, uc14)
	botResponses = append(botResponses, botResponses18)
	botbotResponses = append(botbotResponses, adminResults2)

	adminResults1, chatResults := cube_bet_router.Route(ctx, db, uc13)
	botResponses = append(botResponses, chatResults)
	botbotResponses = append(botbotResponses, adminResults1)

	// !dropeffect
	// !passthejester
	botbotResponses1, websocketResponses, botResponses17, audioRequests5 := streamgod_router.Route(
		ctx,
		db,
		uc11,
	)
	botResponses = append(botResponses, botResponses17)
	botbotResponses = append(botbotResponses, botbotResponses1)
	audioRequests = append(audioRequests, audioRequests5)

	// !pokemon
	ars1, botResponses11 := filter.RoutePokemonCommands(ctx, db, uc7)
	audioRequests = append(audioRequests, ars1)
	botResponses = append(botResponses, botResponses11)

	// Process audio requests and saves them in the DB to Played later
	audioRequests3, botResponses10, obsRequests := filter.AudioRequests(ctx, db, uc6)
	botResponses = append(botResponses, botResponses10)
	audioRequests = append(audioRequests, audioRequests3)

	// !color
	botResponses3 := colorscheme_router.Route(ctx, db, uc1, themes)
	botResponses = append(botResponses, botResponses3)

	// !props
	botResponses4 := economy_router.PropsRouter(ctx, db, uc10)
	botResponses = append(botResponses, botResponses4)

	// !perms
	botResponses14 := economy_router.PermsRouter(ctx, db, uc9)
	botResponses = append(botResponses, botResponses14)

	// !buy
	botResponses13 := economy_router.BuyRoute(ctx, db, uc2)
	botResponses = append(botResponses, botResponses13)

	// !me
	// !jester
	// !formparty
	// !parties
	// !join
	botResponses9 := economy_router.MeRoute(ctx, db, uc5)
	botResponses = append(botResponses, botResponses9)

	// !upload
	botbotResponses2, botResponses19 := media_router.Route(ctx, db, uc15)
	fmt.Printf("botbotResponses2 = %+v\n", botbotResponses2)
	fmt.Printf("botResponses19 = %+v\n", botResponses19)
	botbotResponses3, botResponses20, obsResponses1 := media_request_processor.Process(ctx, db)
	fmt.Printf("botResponses3 = %+v\n", botbotResponses3)
	fmt.Printf("botResponses20 = %+v\n", botResponses20)
	fmt.Printf("obsResponses1 = %+v\n", obsResponses1)

	// !soundeffect
	// !requests
	// !approve
	// !deny
	botbotResponses2, botResponses5 := soundeffect_request_router.Route(ctx, db, uc3)
	botResponses = append(botResponses, botResponses5)
	botbotResponses = append(botbotResponses, botbotResponses2)

	// !love
	// !hate
	// !props
	botResponses8 := relationship_manager.Manage(ctx, db, uc4)
	botResponses = append(botResponses, botResponses8)

	// This runs every second to process any new soundeffect requests
	botResponses6, audioRequests2 := soundeffect_request_processor.Process(ctx, db)
	botResponses = append(botResponses, botResponses6)
	audioRequests = append(audioRequests, audioRequests2)

	// Drops Mana and Street Cred
	// Also Leaks credentials ssshhh
	if *isMarketOpen {
		botResponses7 := hand_of_the_market.Serve(ctx, db)
		botResponses = append(botResponses, botResponses7)
	}

	// !steal
	botResponses12 := criminal_activities.Serve(ctx, db, uc12)
	botResponses = append(botResponses, botResponses12)

	// Fan-In the User Soundeffect Requests and the Automatic Theme songs
	// Then pass them to the soundboard to execute them
	playSoundRequests := utils.AudioRequestFanIn(ctx, audioRequests...)

	// This is what actually plays the sounds
	botResponses2, notifications1, obsRequests2 := soundboard.Execute(
		ctx,
		db,
		playSoundRequests)
	botResponses = append(botResponses, botResponses2)

	if *connectToOBS {
		obsCommands := tee.ChatFanIn(uc8, obsThemes, obsRequests, obsRequests2, obsResponses1)
		botResponses16 := obs.TrollBegin(db, obsCommands)
		botResponses = append(botResponses, botResponses16)
	}

	// Fan in all the Bot Responses before Reporting
	allResults := utils.StringFanIn(ctx, botResponses...)

	// r1, r2 := tee.FanOutString(allResults)
	// Ahh the other type of message ahhhhhhhhh
	if *enableWebsocketNotifs {
		go notifs.LaunchNotifications(websocketResponses)
	}

	// Pass some messages in here
	// Also send another internal channel
	// We some type of request

	// TODO: Add flag to turn this off
	// Send beginbotbot responses back to Twitch Chat
	reporter.Report(ctx, allResults, &c)

	// Fan
	allBotBotResponses := utils.StringFanIn(ctx, botbotResponses...)
	reporter.Report(ctx, allBotBotResponses, &botChannelConfig)

	// reporter.Report(ctx, allResults, &botChannelConfig)

	// So we should only take in notifications here
	go notification_site.Serve(ctx, notifications1)

	for {
		select {
		case <-ctx.Done():
		default:
		}
	}
}
