package chat

import (
	"fmt"
	"time"

	"gorm.io/gorm"
)

type ChatMessage struct {
	PlayerName string
	PlayerID   *uint
	Streamlord bool `gorm:"-"`
	Streamgod  bool `gorm:"-"`
	StreetCred uint `gorm:"-"`
	CoolPoints uint `gorm:"-"`
	Message    string
}

func Count(db *gorm.DB) int64 {
	var count int64
	db.Table("chat_messages").Count(&count)
	return count
}

func Save(db *gorm.DB, chat_message ChatMessage) error {
	db = db.Create(&chat_message)
	if db.Error != nil {
		fmt.Printf("Save Error = %+v\n", db.Error)
		return db.Error
	}

	return nil
}

type StreetCredMap struct {
	Name       string
	ID         uint
	LoverCount uint
}

// This Query could not be return every user with Their lover sound
func FetchStreetCredCounts(db *gorm.DB) []*StreetCredMap {
	var results []*StreetCredMap

	db.Table("players").Raw(`
	SELECT p.name, p.id, (count(pl) + 1) as lover_count FROM players p
	LEFT JOIN players_lovers pl ON p.ID = pl.lover_id
	INNER JOIN (
	  SELECT
	    COUNT(1), player_id
	    FROM chat_messages
	    WHERE chat_messages.created_at < (NOW() + interval '4 hour')
	    GROUP BY player_id
	) cm ON cm.player_id = p.id
	GROUP BY p.name, p.id
	ORDER BY lover_count DESC`).Scan(&results)

	return results
}

func GiveStreetCredToRecentChatters(db *gorm.DB) []*StreetCredMap {
	results := FetchStreetCredCounts(db)
	for _, r := range results {

		// this needs to be Raw
		tx := db.Exec(`
			UPDATE players
			SET street_cred = street_cred + ?
			WHERE id = ?`, r.LoverCount, r.ID)

		if tx.Error != nil {
			fmt.Printf("res.Error = %+v\n", tx.Error)
		}

	}
	return results
}

// func ChattedSince(db *gorm.DB) []ChatMessage {
// 	timer := time.Now().Add(-(time.Minute * 5))
// 	var cm []ChatMessage
// 	db = db.Where("created_at > ?", timer).Find(&cm)
// 	res := db.Exec("UPDATE players SET mana = mana + 1")
// 	if res.Error != nil {
// 		fmt.Printf("res.Error = %+v\n", res.Error)
// 	}
// 	if db.Error != nil {
// 		return cm
// 	}
// 	fmt.Printf("chat_message = %+v\n", cm)
// 	return cm
// }

func HasChattedToday(db *gorm.DB, playername string) bool {
	// TODO: Make this configurable
	fourHours := time.Now().Add(-(time.Hour * 4))

	var cm ChatMessage
	db = db.Where("player_name = ? AND created_at > ?", playername, fourHours).
		First(&cm)

	// Should this return an error and should we report it???
	if db.Error != nil {
		// TODO: Do we need this error
		fmt.Printf("%s Hasn't Chatted Today: | %+v\n", playername, db.Error)
		return false
	}

	return true
}
