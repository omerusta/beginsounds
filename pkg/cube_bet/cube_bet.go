package cube_bet

import (
	"fmt"
	"math"
	"time"

	"github.com/lib/pq"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

type CubeGame struct {
	ID        uint
	SolveTime int
	StartedAt *time.Time
	DeletedAt *time.Time
}

type CubeBet struct {
	ID         uint
	Duration   int
	PlayerID   uint
	CubeGameID uint
}

type CubeBetStreamCommand struct {
	ID              uint
	CubeBetID       uint
	StreamCommandID uint
}

type PlayerBet struct {
	CubeBet
	Bets pq.StringArray `gorm:"type:text[]"`
	// Bets []string `gorm:"type:text[]"`
	// Bets []string `gorm:"<-:false"`
}

func FindPlayerIDFromBet(db *gorm.DB, cubeBetStreamCommandID uint) uint {
	var playerID uint
	db.Raw(`
		SELECT
			p.ID
		FROM
			cube_bet_stream_commands cbsc
		WHERE
			cbsc.id = ?
		INNER JOIN
			cube_bets cb
		ON
			cbsc.cube_bet_id = cb.id
		INNER JOIN
			players p
		ON
			cb.player_id = p.id
	`, cubeBetStreamCommandID).Scan(&playerID)
	return playerID
}

func ProcessWinner(
	db *gorm.DB,
	chatResults chan<- string,
	w *player.Player,
	cg *CubeGame,
) string {

	var winnerBet CubeBet
	db.Where("cube_game_id = ?", cg.ID).First(&winnerBet)

	winnerRes := []CubeBetStreamCommand{}
	db.Table("cube_bet_stream_commands").
		Where("cube_bet_id = ?", winnerBet.ID).
		Scan(&winnerRes)

	loserRes := []CubeBetStreamCommand{}
	db.Table("cube_bet_stream_commands").
		Where("cube_bet_id != ?", winnerBet.ID).
		Scan(&loserRes)

	for i := 0; i < len(winnerRes); i++ {
		prize := loserRes[i]
		playerID := FindPlayerIDFromBet(db, prize.ID)

		var sc stream_command.StreamCommand
		db.Where("id = ?", prize.StreamCommandID).First(&sc)

		msg := fmt.Sprintf("@%s Won %s", w.Name, sc.Name)
		chatResults <- msg

		player.TransferCommand(db, prize.StreamCommandID, playerID, winnerBet.PlayerID)
	}

	fmt.Printf("winnerRes = %+v\n", winnerRes)
	fmt.Printf("loserRes = %+v\n", loserRes)

	return fmt.Sprintf("%d Sounds Won", len(winnerRes))
}

func FindWinner(db *gorm.DB, solveTime int) *player.Player {
	// 1: Write the SQL to find the first value sorted by
	//    created_at, closet but not over the solvetime

	// 2: get all the bets and do it code
	var bets []CubeBet
	db.Order("duration").Order("created_at asc").Find(&bets)

	var closestGuess CubeBet
	lastOffset := 10000000

	// 45 43, 45 46
	// CUBE: 45
	// BET 44 <- first to bet
	// BET 44 <-
	// BET 46
	for _, bet := range bets {
		offset := int(math.Abs(float64(solveTime - bet.Duration)))
		if offset < lastOffset {
			closestGuess = bet
			lastOffset = offset
		}
	}

	p := player.FindByID(db, closestGuess.PlayerID)
	fmt.Printf("closestGuess = %+v\n", closestGuess)
	return p
}

func CurrentRunningGame(db *gorm.DB) *CubeGame {
	var cg CubeGame

	tx := db.Where("deleted_at IS NULL AND started_at IS NOT NULL").
		Order("created_at desc").First(&cg)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	return &cg
}

func CurrentGame(db *gorm.DB) *CubeGame {
	var cg CubeGame

	tx := db.Where("deleted_at IS NULL").Order("created_at desc").First(&cg)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	return &cg
}

func AllBets(db *gorm.DB) []PlayerBet {
	var results []PlayerBet

	db.Raw(`
		SELECT
			cb.id, cb.duration, cb.player_id, array_agg(sc.name) as bets
		FROM
			cube_bets cb
		INNER JOIN
			cube_bet_stream_commands b
		ON
			cb.ID = b.cube_bet_id
		INNER JOIN
			stream_commands sc
		ON
			b.stream_command_id = sc.id
		GROUP BY
  		cb.id, cb.duration, cb.player_id
	`).Scan(&results)

	return results
}

func (c *CubeBet) String() string {
	return fmt.Sprintf(
		"CubeBet<ID: %d, Duration: %d, PlayerID: %d>",
		c.ID,
		c.Duration,
		c.PlayerID,
	)
}

func (c *CubeBetStreamCommand) String() string {
	return fmt.Sprintf(
		"CubeBetStreamCommand<CubeBetID: %d, StreamCommandID: %d>",
		c.CubeBetID,
		c.StreamCommandID,
	)
}

func NewBet(db *gorm.DB, duration int, playerID uint) *CubeBet {
	cb := CubeBet{
		Duration: duration,
		PlayerID: playerID,
	}
	tx := db.Create(&cb)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	return &cb
}

//
func ProcessBet(db *gorm.DB, pb *parser.ParsedCubeBet) (*CubeBet, []*CubeBetStreamCommand, error) {
	bets := []*CubeBetStreamCommand{}
	cg := CurrentGame(db)
	cb := CubeBet{
		Duration:   pb.Duration,
		PlayerID:   pb.PlayerID,
		CubeGameID: cg.ID,
	}
	tx := db.Create(&cb)
	if tx.Error != nil {
		return &CubeBet{}, bets, tx.Error
	}

	for _, command := range pb.Commands {
		c := stream_command.Find(db, command)

		if c.ID == 0 {
			fmt.Printf("Error Finding Command: %+v\n", c)
			continue
		}

		b := CubeBetStreamCommand{
			CubeBetID:       cb.ID,
			StreamCommandID: c.ID,
		}
		tx := db.Create(&b)
		if tx.Error != nil {
			fmt.Printf("Error Creating Command Bet: %v | %+v\n", b, tx.Error)
			continue
		}

		bets = append(bets, &b)
	}

	return &cb, bets, nil
}
