package cube_bet_router

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/cube_bet"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gorm.io/gorm"
)

func Route(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	adminResults := make(chan string)
	chatResults := make(chan string)

	go func() {
		defer close(adminResults)
		defer close(chatResults)

	Loop:
		for msg := range messages {
			parsedCmd, invalidCmds, unownedCmds, err := parser.ParseCubeBet(db, &msg)
			if err != nil {
				fmt.Printf("Error Parsing Bet: %+v\n", err)
			}

			if len(invalidCmds) != 0 || len(unownedCmds) != 0 {
				m := fmt.Sprintf("@%s Invalid Cmds: %v | Unowned Cmds: %v", msg.PlayerName, invalidCmds, unownedCmds)
				adminResults <- m
			}

			if len(invalidCmds) > 0 {
				m := strings.Join(invalidCmds, ", ")
				chatResults <- fmt.Sprintf("@%s Invalid Commands in !bet: %s", msg.PlayerName, m)
			}

			if len(unownedCmds) > 0 {
				m := strings.Join(unownedCmds, ", ")
				chatResults <- fmt.Sprintf("@%s Betting Commands you Don't Own!: %s", msg.PlayerName, m)
			}

			select {
			case <-ctx.Done():
				return
			default:

				if parsedCmd.Name == "!bet" {
					cg1 := cube_bet.CurrentGame(db)
					if cg1.ID == 0 {
						chatResults <- fmt.Sprintf("@%s Begin isn't planning on solving a Cube Currently", msg.PlayerName)
						continue Loop
					}

					cg := cube_bet.CurrentRunningGame(db)
					if cg.ID != 0 {
						chatResults <- fmt.Sprintf("@%s NO BETTING WHILE BEGIN IS CUBING", msg.PlayerName)
						continue Loop
					}

					cb, commands, err := cube_bet.ProcessBet(db, parsedCmd)
					if err != nil {
						chatResults <- fmt.Sprintf("@%s Error Taking Bet: %+v", msg.PlayerName, err)
						continue Loop
					}
					msg := fmt.Sprintf(
						"Thanks for the bet @%s - %ds | %d Commands",
						msg.PlayerName,
						cb.Duration,
						len(commands),
					)
					chatResults <- msg
				}
			}

			parser2, _, _ := parser.ParseChatMessageNoJudgements(db, &msg)

			if parser2.Name == "cubed" && msg.PlayerName == "beginbotbot" {

				cg := cube_bet.CurrentRunningGame(db)

				deleteTime := time.Now()
				tx := db.Model(&cg).
					Where("ID = ?", cg.ID).
					Updates(cube_bet.CubeGame{DeletedAt: &deleteTime, SolveTime: parsedCmd.SolveTime})

				if tx.Error != nil {
					fmt.Printf("tx.error = %+v\n", tx.Error)
				}

				winner := cube_bet.FindWinner(db, parsedCmd.SolveTime)

				// We need to give out the winners prizes
				res := cube_bet.ProcessWinner(db, adminResults, winner, cg)

				solvedMsg := fmt.Sprintf(
					"Cube Solved! %d - Winner: @%s | %s",
					parsedCmd.SolveTime, winner.Name, res,
				)
				chatResults <- solvedMsg
			}

			if parser2.Name == "bettingtime" && msg.PlayerName == "beginbotbot" {
				cg := cube_bet.CubeGame{}
				tx := db.Create(&cg)
				if tx.Error != nil {
					fmt.Printf("Bettingtime Error = %+v\n", tx.Error)
					continue Loop
				}
				solvedMsg := "Get you bets in people with !bet SECONDS_TO_SOLVE_CUBE (Ex !bet 45)"
				chatResults <- solvedMsg
			}

			if parser2.Name == "new_cube" && msg.PlayerName == "beginbotbot" {
				// This needs to set the started_at
				// look up the current cube
				// and set its start at
				// We need to start new a
				// This needs to set

				cg := cube_bet.CurrentGame(db)
				startTime := time.Now()
				db.Model(&cg).Update("started_at", &startTime)

				solvedMsg := "Cube Solve STARTED NO MORE BETS!"
				chatResults <- solvedMsg
			}

		}

	}()

	return adminResults, chatResults
}
