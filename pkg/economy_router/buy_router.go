package economy_router

import (
	"context"
	"fmt"
	"math/rand"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/sound_store"
	"gorm.io/gorm"
)

func BuyRoute(ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			if cmd == "!buy" {
				parsedCmd, _, pe := parser.ParseChatMessageNoJudgements(db, &msg)
				if pe != nil {
					results <- fmt.Sprintf("Error during parsing !buy command %v", pe)
					continue MsgLoop
				}

				// TODO: Look up this number
				commandTotal := 2500
				if parsedCmd.TargetAmount > commandTotal {
					results <- fmt.Sprintf("@%s You can't buy more commands than exist: %d",
						msg.PlayerName, commandTotal)
					continue MsgLoop
				}

				// Why do we look up choices soo early???
				// This will return 3
				// we asked for 10
				// so we are exiting early
				choices := RandomCommand(db, msg.CoolPoints, *msg.PlayerID)
				// The total price is important as well
				// if len(choices) < 1 || len(choices) < parsedCmd.TargetAmount {
				if len(choices) < 1 {
					results <- fmt.Sprintf(
						"No sounds found for purchase: @%s. Cool Points: %d",
						msg.PlayerName,
						msg.CoolPoints,
					)
					continue MsgLoop
				}

				var totalAvailableCost uint
				for _, c := range choices {
					totalAvailableCost += c.Cost
				}
				canAffordEverything := totalAvailableCost <= msg.CoolPoints
				if canAffordEverything {
					var cmds []string
					for _, c := range choices {
						cmds = append(cmds, c.Name)
					}
					boughtCmds := strings.Join(cmds, ", ")
					msg := fmt.Sprintf("@%s spent all their money on %s: 3 Cool Points",
						msg.PlayerName, boughtCmds)
					results <- msg
					continue MsgLoop
				}

				if parsedCmd.TargetAmount > 1 {
					// We could make a better structure
					var boughtCmds []string
					var total uint

					// Is this is above an amount we should be it in a more intelligent way
					// We are not checking the amount for the user
					// on each command
					initialCoolPoints := msg.CoolPoints

					// I need a range,
					// that I
					// randoChoices := shuffleSlice(choices)

				BuyLoop:
					for i := 0; i < parsedCmd.TargetAmount; i++ {
						if len(choices) < 1 {
							break BuyLoop
						}

						randomIndex := rand.Intn(len(choices))
						c := choices[randomIndex]
						choices = RemoveCommand(choices, randomIndex)

						initialCoolPoints = initialCoolPoints - c.Cost

						if initialCoolPoints < 1 {
							break BuyLoop
						}

						if total+c.Cost >= msg.CoolPoints {
							continue BuyLoop
						}

						result := sound_store.Buy(db, msg.PlayerName, c.Name)
						if result != "" {
							total += c.Cost
							boughtCmds = append(boughtCmds, c.Name)
						}
					}

					cmds := strings.Join(boughtCmds, ", ")
					if len(boughtCmds) > 20 {
						cmds = fmt.Sprintf("%d Commands", len(boughtCmds))
					}

					msg := fmt.Sprintf("@%s bought: %v for %d Cool Points",
						msg.PlayerName, cmds, total)
					results <- msg
					continue MsgLoop
				}

				if parsedCmd.TargetCommand != "" {
					result := sound_store.Buy(db, msg.PlayerName, parsedCmd.TargetCommand)
					if result != "" {
						results <- result
					}
					continue MsgLoop
				}

				randomIndex := rand.Intn(len(choices))
				c := choices[randomIndex]

				result := sound_store.Buy(db, msg.PlayerName, c.Name)
				if result != "" {
					results <- result
				}
				continue MsgLoop
			}

		}
	}()

	return results
}

func RemoveCommand(s []RandoComand, i int) []RandoComand {
	s[i] = s[len(s)-1]
	// We do not need to put s[i] at the end, as it will be discarded anyway
	return s[:len(s)-1]
}
