package economy_router

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/party"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

func PermsRouter(
	ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			// We need to generate webpage on perm check
			if cmd == "!perms" || cmd == "!stats" {
				pe, parsedCmd := parser.ParseChatMessage(db, &msg)
				if pe != nil {
					results <- fmt.Sprintf("Error Fetching !perms %v", pe)
					continue MsgLoop
				}

				isTheme := parsedCmd.TargetCommand != parsedCmd.TargetUser
				if parsedCmd.TargetCommand != "" && isTheme {
					res := CommandInfo(db, msg, parsedCmd.TargetCommand)
					results <- res
					continue MsgLoop
				}

				results <- UserInfo(db, parsedCmd.TargetUser)
				continue MsgLoop
			}

		}
	}()

	return results
}

func MeRoute(ctx context.Context, db *gorm.DB, commands <-chan chat.ChatMessage) <-chan string {
	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))
			parsedCmd, parts, _ := parser.ParseChatMessageNoJudgements(db, &msg)

			if cmd == "!css" {
				if len(parts) == 2 {
					u, err := url.ParseRequestURI(parts[1])
					if err != nil {
						results <- fmt.Sprintf("@%s We need a valid URL to RAW CSS", msg.PlayerName)
					}

					fmt.Printf("Downloading CSS: %+v\n", u)
					err = downloadFile(fmt.Sprintf("assets/static/%s.css", msg.PlayerName), parts[1])
					if err != nil {
						results <- fmt.Sprintf("@%s Error Updating CSS %+v", msg.PlayerName, err)
						continue MsgLoop
					}

					website := fmt.Sprintf("https://beginworld.website-us-east-1.linodeobjects.com/users/%s.html", strings.ToLower(msg.PlayerName))
					results <- fmt.Sprintf("@%s Thank you for your css, your page will be updated shortly!", msg.PlayerName)
					results <- website
					website_generator.SyncPlayerCSS(msg.PlayerName)
				}
			}

			if cmd == "!formparty" {
				name := parts[1]
				manifesto := strings.Join(parts[2:], " ")

				p := party.Party{
					Name:      name,
					Manifesto: manifesto,
					LeaderID:  *msg.PlayerID,
				}
				fmt.Printf("p = %+v\n", p)
				tx := db.Create(&p)
				if tx.Error != nil {
					fmt.Printf("tx.Error = %+v\n", tx.Error)
					continue MsgLoop
				}
				results <- fmt.Sprintf("New Party Under Review: %s", p.Name)
			}

			if cmd == "!leaveparty" {
				p, err := party.LeaveParty(db, *msg.PlayerID)
				if err != nil || p.ID == 0 {
					fmt.Printf("Error Leaving Party: %+v\n", err)
					continue MsgLoop
				}
				results <- fmt.Sprintf("%s Left Party: %s", msg.PlayerName, p.Name)
			}

			if cmd == "!joinparty" {
				isMember := party.IsMemberOfAnyParty(db, *msg.PlayerID)
				if isMember {
					results <- fmt.Sprintf("@%s ALREADY IN PARTY", msg.PlayerName)
					continue MsgLoop
				}
				if len(parts) < 2 {
					results <- fmt.Sprintf("@%s You need to specify which party to join", msg.PlayerName)
					continue MsgLoop
				}

				name := parts[1]
				party.JoinParty(db, name, *msg.PlayerID)
				results <- fmt.Sprintf("@%s joined: %s", msg.PlayerName, name)
			}

			if cmd == "!parties" {
				msg := "https://beginworld.website-us-east-1.linodeobjects.com/parties.html"
				results <- msg
				website_generator.CreatePartiesPage(db)
				website_generator.SyncPartiesSite()

				// for _, party := range parties {
				// 	p := player.FindByID(db, party.LeaderID)
				// 	members := strings.Join(party.MemberNames(db), ", ")
				// 	partyMsg := fmt.Sprintf("%d: %s - %s | Leader: %s | Members: %s", party.ID, party.Name, party.Manifesto, p.Name, members)
				// 	results <- partyMsg
				// }
			}

			if cmd == "!so" {
				if parsedCmd.TargetUser != "" {
					msg := fmt.Sprintf("Shoutout twitch.tv/%s", parsedCmd.TargetUser)
					results <- msg
				}
			}

			// We need to concat this list
			if cmd == "!jester" {
				jester := stream_jester.CurrentJester(db)

				if jester.PlayerID == nil {
					results <- "No current Jester!"
					continue MsgLoop
				}

				p := player.FindByID(db, *jester.PlayerID)
				cmds := strings.Join(stream_jester.Commands, " ")
				msg := fmt.Sprintf("Jester: @%s | %v", p.Name, cmds)
				results <- msg
			}

			if cmd == "!me" {
				// We need to look up the poetical party
				userInfo := UserInfo(db, msg.PlayerName)
				party := party.FindForPlayerID(db, *msg.PlayerID)
				partyName := party.Name
				if party.ID == 0 {
					partyName = "Not associated w/ a Party"
				}
				resMsg := userInfo + fmt.Sprintf(" | Party: %s", partyName)
				// results <- userInfo
				results <- resMsg

				go func() {
					p2 := player.Find(db, msg.PlayerName)
					website_generator.CreateUserPage(db, p2)
					website_generator.SyncUserSite(p2.Name)
				}()

				continue MsgLoop
			}

		}
	}()

	return results
}

// Might not find the static directory at the root
// We also might want to add some validation, around being Raw CSS
// We could approved RAW CSS Sites
//
// Pastebin
// Gitlab Snippets
// Github Gists
func downloadFile(filepath string, url string) (err error) {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
