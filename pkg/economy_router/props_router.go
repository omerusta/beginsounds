package economy_router

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/prop_department"
	"gorm.io/gorm"
)

func PropsRouter(
	ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			if cmd == "!props" {
				fmt.Println("\t\tInside of Props")
				// This handles street cred as well which is an issue
				pe, parsedCmd := parser.ParseChatMessage(db, &msg)
				if pe != nil {
					results <- fmt.Sprintf("Error Giving Props !props %v", pe)
					continue MsgLoop
				}

				p := player.FindByID(db, *msg.PlayerID)
				res, err := prop_department.GiveProps(db, p, parsedCmd)
				if err != nil {
					m := fmt.Sprintf("%v", err)
					results <- m
					continue MsgLoop
				}

				results <- res
				continue MsgLoop
			}

		}
	}()

	return results
}
