package economy_router

import "gorm.io/gorm"

type RandoComand struct {
	ID   uint
	Name string
	Cost uint
}

// Is this theme filtering working here?
func RandomCommand(db *gorm.DB, coolPoints uint, playerID uint) []RandoComand {
	var results []RandoComand
	db.Table("stream_commands").Raw(
		`SELECT
  		sc.id, sc.name as name, sc.cost as cost
		FROM
  		stream_commands sc
		WHERE
				sc.cost <= ?
		AND
  		sc.id NOT IN (
			SELECT
				stream_commands.id
			FROM
				stream_commands
			INNER JOIN
				commands_players
			ON
				stream_commands.id = commands_players.stream_command_id
			WHERE
				commands_players.player_id = ?
			AND
				sc.theme = false)`, coolPoints, playerID).Scan(&results)
	return results
}

func RandomCommandNoCost(db *gorm.DB, playerID uint) []RandoComand {
	var results []RandoComand
	db.Table("stream_commands").Raw(
		`SELECT
  		sc.id, sc.name as name, sc.cost as cost
		FROM
  		stream_commands sc
		WHERE
  		sc.id NOT IN (
			SELECT
				stream_commands.id
			FROM
				stream_commands
			INNER JOIN
				commands_players
			ON
				stream_commands.id = commands_players.stream_command_id
			WHERE
				commands_players.player_id = ?
			AND
				sc.theme = false)`, playerID).Scan(&results)
	return results
}
