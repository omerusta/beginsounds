package filter

import (
	"fmt"
	"testing"
	"time"

	"context"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestFilter(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	ctx := context.Background()

	messages := make(chan chat.ChatMessage, 3)

	// Just a Chat Message
	cm := chat.ChatMessage{
		PlayerName: "beginbot",
		Message:    "Very Cool"}
	messages <- cm

	// This should be a audio request
	cm1 := chat.ChatMessage{
		PlayerName: "beginbot",
		Message:    "!damn"}
	messages <- cm1

	// This should be a audio request
	cm2 := chat.ChatMessage{
		PlayerName: "beginbot",
		Message:    "!pokemon"}
	messages <- cm2

	commands, msgs := RouteUserCommands(ctx, db, messages)

	fmt.Printf("commands = %+v\n", commands)
	fmt.Printf("msgs = %+v\n", msgs)

	// TODO: I hate this
	time.Sleep(time.Millisecond * 50)

	select {
	case _ = <-commands:
	default:
		t.Errorf("We did not get a Command back")
	}

	// select {
	// case msg = <-msgs:
	// 	if msg != "Whose that pokemon" {

	// 	}
	// default:
	// 	t.Errorf("We did not get a Command back")
	// }
}
