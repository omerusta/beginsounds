package irc

import (
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/config"
)

func SendMsg(c *config.Config, msg string) {
	fmt.Fprintf(c.Conn, "PRIVMSG #%s :%s\r\n", c.Channel, msg)
}

func Pong(c *config.Config) {
	fmt.Fprintf(c.Conn, "PONG\r\n")
}
