package media_parser

import (
	"errors"
	"fmt"
	"net/url"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
)

// Parse parses the msg to save a Media Request
func Parse(mediaType string, msg chat.ChatMessage) (*media_request.MediaRequest, error) {
	var isApproved bool
	if msg.Streamgod || msg.Streamlord {
		isApproved = true
	}
	request := media_request.MediaRequest{
		MediaType:   mediaType,
		Approved:    isApproved,
		Requester:   msg.PlayerName,
		RequesterID: msg.PlayerID,
	}
	parts := strings.Split(msg.Message, " ")

	_, err := url.ParseRequestURI(parts[1])
	if err != nil {
		em := fmt.Sprintf("@%s passed invalid URL %+v\n", msg.PlayerName, err)
		return &media_request.MediaRequest{}, errors.New(em)
	}
	request.Url = parts[1]

	// If we don't include a command name
	// We assume its the Theme Song
	if len(parts) > 2 {
		for _, part := range parts[2:] {
			if utils.IsTimeStamp(part) && request.StartTime == "" {
				request.StartTime = part
			} else if utils.IsTimeStamp(part) {
				request.EndTime = part
			} else {
				request.Name = part
			}
		}
	}

	// If we haven't found a command name yet
	// Assume it's the Players Name
	if request.Name == "" {
		request.Name = msg.PlayerName
	}

	return &request, nil
}
