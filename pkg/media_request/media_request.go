package media_request

import (
	"time"

	"gorm.io/gorm"
)

type MediaRequest struct {
	ID          uint
	Url         string
	MediaType   string
	Name        string
	StartTime   string
	DeletedAt   *time.Time
	EndTime     string
	Requester   string `gorm:"-"`
	RequesterID *uint
	Approved    bool   // 1 byte
	Approver    string `gorm:"-"`
	ApproverId  *uint
}

func FindByName(db *gorm.DB, imageName string) (*MediaRequest, error) {
	var result MediaRequest
	tx := db.Model(&MediaRequest{}).
		Where("media_type = 'image' AND approved IS true AND deleted_at IS NULL").
		Scan(&result)
	return &result, tx.Error
}

// UnapprovedImages returns all Media Request with an media_type of image
// 	and are unapproved
func UnapprovedImages(db *gorm.DB) []MediaRequest {
	var results []MediaRequest

	db.Model(&MediaRequest{}).
		Where("media_type = 'image' AND approved IS false").
		Scan(&results)

	return results
}

// ApprovedImages returns all the approved Media Requests
// that have the media_type image
func ApprovedImages(db *gorm.DB) ([]MediaRequest, error) {
	var results []MediaRequest

	tx := db.Model(&MediaRequest{}).
		Where("media_type = 'image' AND approved IS true AND deleted_at IS NULL").
		Scan(&results)

	return results, tx.Error
}
