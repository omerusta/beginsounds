package media_request

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
)

func TestApprovedRequests(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	images, err := ApprovedImages(db)
	if err != nil {
		t.Errorf("FREAKING OUT %+v", err)
	}
	if len(images) != 0 {
		t.Errorf("FREAKING OUT %d", len(images))
	}
}
