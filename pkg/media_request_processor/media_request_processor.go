package media_request_processor

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gorm.io/gorm"
)

// Process looks for approved Media Request
// 	attempts to download them and make them
// available on stream
func Process(
	ctx context.Context,
	db *gorm.DB,
) (<-chan string, <-chan string, <-chan chat.ChatMessage) {

	timer := time.NewTicker(time.Second * 5)
	chatResults := make(chan string)
	botbotResults := make(chan string)
	obsResults := make(chan chat.ChatMessage)

	go func() {
		defer close(chatResults)
		defer close(botbotResults)
		defer close(obsResults)

		fmt.Println("Launching The Media Processor")

	MsgLoop:
		for {
			select {
			case <-ctx.Done():
				return
			default:
				// We need to grab all the image requests are approved
				// This should be looking for approved
				// This should never return requests that are deleted
				images, err := media_request.ApprovedImages(db)
				if err != nil {
					continue MsgLoop
				}

				for _, image := range images {
					fmt.Printf("\t\t === Image for Downloading = %+v\n", image)

					err := downloadFile(image.Url, image.Name)
					if err != nil {
						fmt.Printf("err Downloading: %s | %s %+v\n", image.Name, image.Url, err)
						continue MsgLoop
					}

					fmt.Printf("\t $$$ CreateSource %+v\n", image.Name)

					m := fmt.Sprintf("!createsource %s", image.Name)
					cm := chat.ChatMessage{
						Message:    m,
						PlayerName: "beginbotbot",
						Streamgod:  true,
						Streamlord: true,
					}
					fmt.Printf("\t\t ~~~ cm = %+v\n", cm)
					obsResults <- cm
				}

				<-timer.C
			}
		}
	}()

	return botbotResults, chatResults, obsResults
}

func downloadFile(url string, name string) error {
	//Get the response bytes from the url
	response, err := http.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return errors.New("Received non 200 response code")
	}
	//Create a empty file
	// Where do we want to save these
	// Probably the similar to where we save our audio folder
	fileName := fmt.Sprintf("/home/begin/stream/Stream/ViewerImages/%s.png", name)
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	//Write the bytes to the fiel
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	return nil
}
