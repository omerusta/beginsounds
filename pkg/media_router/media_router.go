package media_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_parser"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gorm.io/gorm"
)

// Route the commands related to uploading
//  and approving Media in BeginWorld™
func Route(
	ctx context.Context,
	db *gorm.DB,
	msgs <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	chatResults := make(chan string)
	botbotResults := make(chan string)

	go func() {
		defer close(chatResults)
		defer close(botbotResults)

	MsgLoop:
		for msg := range msgs {
			select {
			case <-ctx.Done():
				return

			default:
				parsedCmd, _, _ := parser.ParseChatMessageNoJudgements(db, &msg)

				switch parsedCmd.Name {
				case "image":
					media, err := media_parser.Parse("image", msg)
					if err != nil {
						fmt.Printf("Error Parsing = %+v\n", err)
						continue MsgLoop
					}
					fmt.Printf("media = %+v\n", media)

					tx := db.Create(&media)
					if tx.Error != nil {
						fmt.Printf("tx.Error = %+v\n", tx.Error)
					}
				}
			}
		}
	}()

	return botbotResults, chatResults
}
