package notification_site

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

// Hack of all Hacks
func Serve(ctx context.Context, messages <-chan string) {
	r := mux.NewRouter()

	go func() {
		for msg := range messages {
			r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

				// We can pull in dynamic refresh time from the messages herer
				refresh := `<head>
					<link rel="stylesheet" type="text/css" href="https://mygeoangelfirespace.city/styles/style.css">
					<meta http-equiv="refresh" content="0.2">
				</head>`

				w.Write([]byte(refresh + "\n" + msg + "\n"))
			})
		}
	}()

	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:1917",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
