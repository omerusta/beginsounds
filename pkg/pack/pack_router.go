package pack

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

func Route(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	botbotResponses := make(chan string)
	chatResponses := make(chan string)

	go func() {
		for msg := range messages {
			parsedCmd, parts, err := parser.ParseChatMessageNoJudgements(db, &msg)

			if err != nil {
				fmt.Printf(" = %+v\n", err)
			}

			switch parsedCmd.Name {
			case "listpacks":
				names, err := AllNames(db)
				if err == nil {
					site := "http://www.beginworld.exchange/packs.html"
					m := fmt.Sprintf("Packs: %s | %s", strings.Join(names, ", "), site)
					chatResponses <- m
					GeneratePacksPage(db)
					website_generator.SyncRootPage("packs")
				}
			case "createpack":
				p, err := CreatePackFromParts(db, parts[1:])
				// Should we pass this to the pack builder
				// botbotResponses <- "New Pack party"
				if err == nil {
					botbotResponses <- fmt.Sprintf("New Pack: %s", p.Name)
				}
			case "addtopack":
				packName := parts[1]
				cps, _ := AddSoundsToPacks(db, packName, parts[2:])
				for _, command := range cps {
					botbotResponses <- fmt.Sprintf("New Command: %s Added to Pack: %s", command, packName)
				}
				// s = "New Sound in \"party\": !damn"
			case "removefrompack":
				// case "approvepack":
				// case "denypack":

			case "droppack":
				potentialPack := parts[1]
				p, _ := FindByName(db, potentialPack)
				// playa := player.FindByName(db, parsedCmd.TargetUser)
				p.GiveToPlayer(db, parsedCmd.TargetUser)
				chatResponses <- fmt.Sprintf("@%s got pack: %s", parsedCmd.TargetUser, potentialPack)

				// We need the user and the stream command
			}
		}
	}()

	return botbotResponses, chatResponses
}
