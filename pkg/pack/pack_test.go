package pack

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestBuyingPack(t *testing.T) {

}

func TestCreatingPack(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	p, _ := Create(db, "teej")
	fmt.Printf("p = %+v\n", p)

	count := Count(db)
	if count != 1 {
		t.Errorf("I THOUGHT WE WERE CREATING PACKS")
	}

	ps := p.Players(db)
	if len(ps) != 0 {
		t.Errorf("We should have gotten 0 players")
	}
	playa := player.CreatePlayerFromName(db, "young.thug")
	p.GiveToPlayer(db, playa.Name)
	p1, _ := FindByName(db, "teej")
	ps1 := p1.Players(db)
	if len(ps1) != 1 {
		t.Errorf("We should have gotten 1 players")
	}

}

func TestCreatePackFromParts(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	parts := []string{"party"}
	p, _ := CreatePackFromParts(db, parts)
	if p.Name != "party" {
		t.Errorf("Wrong Pack Name: %s", p.Name)
	}

	c := Count(db)
	if c != 1 {
		t.Errorf("We should have one %d", c)
	}

	// We should check theres a part now
}
