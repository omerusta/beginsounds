package parser

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

// How should this relate to the player id and stream command id
func TestIsPing(t *testing.T) {
	data := "PING :tmi.twitch.tv"
	result := IsPing(data)

	if !result {
		t.Error("We did not Find PING")
	}
}

func TestParseCommandTarget(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	cmdName := "damn"
	stream_command.CreateFromName(db, cmdName)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!buy damn"}
	fmt.Printf("c1 = %+v\n", c1)

	_, res := ParseChatMessage(db, &c1)
	if res.TargetCommand != "damn" {
		t.Errorf("Error Parsing Target Command: %s", res.TargetCommand)
	}

	c2 := chat.ChatMessage{PlayerName: username, Message: "!buy gibberish"}

	_, res = ParseChatMessage(db, &c2)
	if res.TargetCommand != "" {
		t.Errorf("Error Parsing Target Command: %s", res.TargetCommand)
	}
}

func TestParseCommandTargetUser(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "young.thug" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}

	c2 := chat.ChatMessage{PlayerName: username, Message: "!props notrealperson"}

	_, res = ParseChatMessage(db, &c2)
	if res.TargetUser != "" {
		t.Errorf("Should have found no user: %s", res.TargetCommand)
	}
}

func TestParseCommandTargetAmount(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")

	p := player.Player{Name: username, StreetCred: 20}
	db.Create(&p)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug 10"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "young.thug" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}
	if res.TargetAmount != 10 {
		t.Errorf("Should have found Target Amount: %d", res.TargetAmount)
	}
}

func TestParseCommandPropsAll(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")
	p := player.Player{Name: username, StreetCred: 20}
	db.Create(&p)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug all"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "young.thug" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}
	if res.TargetAmount != 20 {
		t.Errorf("Should have found Target Amount: %d", res.TargetAmount)
	}
}

func TestParseCommandPropsNegative(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")
	p := player.Player{Name: username, StreetCred: 20}
	db.Create(&p)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug -10"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}
	if res.TargetAmount != 0 {
		t.Errorf("Should have found Target Amount: %d", res.TargetAmount)
	}
}

func TestParseCubeBet(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")
	p := player.Player{Name: username, StreetCred: 20}
	db.Create(&p)
	sc := stream_command.CreateFromName(db, "damn")
	player.AllowAccess(db, p.ID, sc.ID)

	c1 := chat.ChatMessage{
		PlayerName: username,
		PlayerID:   &p.ID,
		Message:    "!bet 10",
	}

	b, _, _, err := ParseCubeBet(db, &c1)

	if err != nil {
		t.Errorf("Error Parsing the Cube Bet %+v\n", err)
	}

	if b.PlayerID != p.ID {
		t.Errorf("We did not parse the Player ID!")
	}

	if b.Duration != 10 {
		t.Errorf("Incorrect Duration Parse %d", b.Duration)
	}
	expectedCommands := []string{"damn"}

	if len(b.Commands) != 1 {
		t.Error("Didn't find any Commands to bet")
		return
	}

	if b.Commands[0] != expectedCommands[0] {
		t.Errorf("Didn't find the Command you bet %v", b.Commands[0])
	}

	// layout := "15:04:05"
	// rawSolveTime, err := time.Parse(layout, part)
	// fmt.Printf("rawSolveTime = %+v\n", rawSolveTime)

	part := "00:00:10"
	layout := "15:04:05"
	parsedTime, _ := time.Parse(layout, part)
	startTime, _ := time.Parse(layout, "00:00:00")
	y := time.Duration(parsedTime.UnixNano() - startTime.UnixNano())
	fmt.Printf("y = %+v\n", y.Seconds())

	c2 := chat.ChatMessage{
		PlayerName: username,
		PlayerID:   &p.ID,
		// Message:    "!cubed 00:00:10",AZjk,,Z
		Message: "!cubed 00:01:01",
	}
	b2, _, _, err := ParseCubeBet(db, &c2)
	if b2.SolveTime != 61 {
		t.Errorf("Didn't Parse the Solve Time Correctly: %d", b.SolveTime)
	}

	// if res.TargetUser != "" {
	// 	t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	// }
	// if res.TargetAmount != 0 {
	// 	t.Errorf("Should have found Target Amount: %d", res.TargetAmount)
	// }
}
