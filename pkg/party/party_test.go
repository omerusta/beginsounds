package party

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestJoinParty(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p := player.CreatePlayerFromName(db, "young.thug")
	op := player.CreatePlayerFromName(db, "lil.uzi")
	fmt.Printf("p = %+v\n", p)

	party := Party{Name: "slime.season",
		Manifesto: "Every in the squad drippin",
		LeaderID:  op.ID,
	}
	db.Create(&party)

	isMember := IsMemberOfAnyParty(db, p.ID)
	if isMember {
		t.Errorf("%s should not be a member of any party yet", p.Name)
	}
	JoinParty(db, "slime.season", p.ID)
	isMember = IsMemberOfAnyParty(db, p.ID)
	if !isMember {
		t.Errorf("%s should be a member of a party ", p.Name)
	}

	names := party.MemberNames(db)
	if len(names) != 1 {
		t.Errorf("Young thug should be a member")
		return
	}

	if names[0] != "young.thug" {
		t.Errorf("Young thug should be a member")
	}
	LeaveParty(db, p.ID)
	isMember = IsMemberOfAnyParty(db, p.ID)
	if isMember {
		t.Errorf("%s should not be a member of any party yet", p.Name)
	}

	// func FindForPlayerID(db *gorm.DB, playerID uint) *Party {

}

func TestLeaveParty(t *testing.T) {
}
