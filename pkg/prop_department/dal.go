package prop_department

import (
	"fmt"

	"gorm.io/gorm"
)

func TransferCredToPoints(
	db *gorm.DB,
	playername string,
	targetUser string,
	targetAmount uint,
) (string, error) {

	res2 := db.Exec(
		`UPDATE players
			SET street_cred = street_cred - ?
			WHERE name = ?`,
		targetAmount, playername)

	if res2.Error != nil {
		fmt.Printf("Error updating Street Cred: %+v\n", res2.Error)
		return "", res2.Error
	}

	res1 := db.Exec(`
			UPDATE players
			SET cool_points = cool_points + ?
			WHERE name = ?`,
		targetAmount, targetUser)

	if res1.Error != nil {
		fmt.Printf("Error updating cool_points: = %+v\n", res1.Error)
		return "", res1.Error
	}

	msg := fmt.Sprintf("@%s Gave @%s %d props", playername, targetUser, targetAmount)
	return msg, nil
}
