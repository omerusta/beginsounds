package prop_department

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/relationship_manager"
	"gorm.io/gorm"
)

func GiveProps(
	db *gorm.DB,
	p *player.Player,
	parsedCmd *parser.ParsedCommand,
) (string, error) {

	var targetAmount int
	if parsedCmd.TargetAmount < 1 {
		targetAmount = 1
	} else {
		targetAmount = parsedCmd.TargetAmount
	}

	if p.StreetCred < targetAmount {
		return "", errors.New(fmt.Sprintf("@%s Not Enough Street Cred!", p.Name))
	}

	targetUser := parsedCmd.TargetUser

	// Set Target User if None Passed in
	// And we are only giving out 1 Street Cred
	if targetAmount == 1 {
		if targetUser == "" {
			lover, err := relationship_manager.RandomLover(db, p.ID)
			if err != nil {
				return "", errors.New(
					fmt.Sprintf("@%s You need to !love someone to give props!", p.Name),
				)
			}
			targetUser = lover
		}
	}

	// If we know the target User Now,
	// then go ahead and give out the creds
	if targetUser == p.Name {
		msg := fmt.Sprintf("@%s you cannot !props yourself!", p.Name)
		return msg, errors.New("You cannot props yourself")
	}

	if targetUser != "" {
		return TransferCredToPoints(db, parsedCmd.Username, targetUser, uint(targetAmount))
	}

	loverNames := relationship_manager.LovedNames(db, p.ID)
	if len(loverNames) == 0 {
		return "", errors.New(
			fmt.Sprintf("@%s You must have lovers for undirected props", p.Name),
		)
	}
	perUser := targetAmount / len(loverNames)
	// rem := len(loverNames) % targetAmount

	for _, lover := range loverNames {
		// We could split, so we remove all Street Cred at once
		TransferCredToPoints(db, parsedCmd.Username, lover, uint(perUser))
	}

	lovers := strings.Join(loverNames, ", ")

	msg := fmt.Sprintf("@%s gave %d Street Cred to lovers: %s", p.Name, perUser, lovers)
	return msg, nil
}
