package reporter

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
)

func Report(ctx context.Context, messages <-chan string, c *config.Config) {
	go func() {
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
				fmt.Println(msg)
				irc.SendMsg(c, msg)
			}
		}
	}()
}
