package sound_store

import (
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

func Buy(db *gorm.DB, playerName string, soundname string) string {
	var result string

	// We have to loot up here again for the freshest cool points count
	p := player.Find(db, playerName)
	c := stream_command.Find(db, soundname)

	if c.ID == 0 {
		return fmt.Sprintf("@%s can't buy !%s they doesn't exist!", playerName, soundname)
	}

	if c.Cost > p.CoolPoints {
		return fmt.Sprintf("@%s can't afford !%s %d/%d", playerName, soundname, p.CoolPoints, c.Cost)
	}

	isAllowed := permissions.IsAllowed(db, p.ID, c.ID)
	if isAllowed {
		return fmt.Sprintf("@%s already owns !%s", playerName, soundname)
	}

	err := permissions.AllowUserAccessToCommand(db, playerName, soundname)
	if err != nil {
		fmt.Print("For some reason the user isn't allowed to play it yet???")
		// Do we want to return full error messages to the Chat
		// return fmt.Sprintf("Error Buying Command: %+v\n", err)
		return ""
	}

	result = fmt.Sprintf("@%s bought !%s", playerName, soundname)
	return result
}
