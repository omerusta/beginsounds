package soundboard

import (
	"log"
	"time"

	"github.com/veandco/go-sdl2/mix"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
)

// The time is for how long until you are cut off
func PlayAudio(audioRequest audio_request.AudioRequest, timer *time.Timer) bool {
	mix.VolumeMusic(1)

	var success bool

	if err := sdl.Init(sdl.INIT_AUDIO); err != nil {
		log.Println("Error initializing SDL: ", err)
		return success
	}

	defer sdl.Quit()

	if err := mix.Init(mix.INIT_MP3); err != nil {
		log.Println("Error initializing Mix: ", err)
		return success
	}

	defer mix.Quit()

	if err := mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
		log.Println(err)
		log.Println("Error Opening Audio: ", err)
		return success
	}
	defer mix.CloseAudio()

	const VOLUME = 10
	mix.VolumeMusic(VOLUME)

	// This is what actually plays the sound
	music, err := mix.LoadMUS(audioRequest.Path)
	if err != nil {
		log.Println(err)
		return success
	}

	ticker := time.NewTicker(100 * time.Millisecond)
	err = music.Play(1)
	if err != nil {
		log.Println(err)
		return success
	}

	success = true

PlayingAudioLoop:
	for {
		<-ticker.C

		select {
		case <-timer.C:
			music.Free()
			break PlayingAudioLoop
		default:
		}

		if !mix.PlayingMusic() {
			music.Free()
			ticker.Stop()
			break
		}
	}
	return success
}
