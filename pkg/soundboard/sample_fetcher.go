package soundboard

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gorm.io/gorm"
)

// TODO: remove Username
type Sample struct {
	Filename string
	Name     string
	Path     string
	Username string
}

func Find(db *gorm.DB, soundname string) *Sample {
	var sample Sample
	res := db.Table("stream_commands").Where("name = ?", soundname).Find(&sample)
	if res.Error != nil {
		fmt.Printf("res.Error = %+v\n", res.Error)
		return &sample
	}
	return &sample
}

func FetchSamples() []Sample {
	var samples []Sample

	samplePath := "/home/begin/stream/Stream/Samples"
	files, err := ioutil.ReadDir(samplePath)

	if err != nil {
		log.Fatal("Could not read Samples directory")
	}

	for _, f := range files {
		if !f.IsDir() {
			s := sampleExtractor(f, samplePath)
			samples = append(samples, s)
		} else {
			res := strings.TrimSpace(f.Name())
			if res == ".git" {
				continue
			}

			newPath := fmt.Sprintf("%s/%s", samplePath, f.Name())
			files2, err := ioutil.ReadDir(newPath)
			if err != nil {
				log.Fatal("Problem reading nested directories")
			}

			for _, f2 := range files2 {
				s := sampleExtractor(f2, newPath)
				samples = append(samples, s)
			}
		}
	}

	return samples
}

func sampleExtractor(f os.FileInfo, samplePath string) Sample {
	e := filepath.Ext(f.Name())
	p := fmt.Sprintf("%s/%s", samplePath, f.Name())
	n := strings.Replace(f.Name(), e, "", 1)
	return Sample{Name: n, Path: p, Filename: f.Name()}
}

func findSoundPathInfo(name string) (string, string) {
	samples := FetchSamples()

	for _, sample := range samples {
		ls := strings.ToLower(name)

		if ls == sample.Name {
			return sample.Filename, sample.Path
		}
	}

	return "", ""
}
