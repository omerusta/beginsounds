package stream_command

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"

	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestIsAllowedToPlay(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	username := "beginbot"
	commandname := "damn"

	sc := CreateFromName(db, commandname)

	res := sc.IsAllowedToPlay(db, username)
	if !res {
		t.Error("User should not be allowed to play")
	}

}

func TestFindCommand(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	fsc := Find(db, "fake_command")

	if fsc.ID != 0 {
		t.Errorf("We found a user when we shouldn't have: %v", fsc)
	}

	sc := CreateFromName(db, "fake_command")
	nsc := Find(db, "fake_command")
	if sc.ID != nsc.ID {
		t.Errorf("We are NOT finding the same Command: %d: %d", sc.ID, nsc.ID)
	}
}

func TestCreateOrUpdate(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	sc := StreamCommand{
		Name: "damn", Cost: 100}
	CreateOrUpdate(db, &sc)
	res := Find(db, "damn")
	if res.Cost != 100 {
		t.Errorf("We are not Finding or Updating Correctly %v", res)
	}

	res = CreateFromName(db, "hello")
	if res.Cost != 1 {
		t.Errorf("Didn't create new Command properly")
	}
}

func TestUpdatingPrice(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	count := Count(db)
	if count != 0 {
		t.Errorf("Users should be cleared: %d", count)
	}

	sc := CreateFromName(db, "fake_command")
	sc = UpdateCost(db, "fake_command", 3)

	count = Count(db)
	if count != 1 {
		t.Errorf("We should have 1 User: %d", count)
	}

	sc.Decay(db)
	if sc.Cost != 2 {
		t.Errorf("Decaying is not working: %d", sc.Cost)
	}

	sc.BoughtPriceIncrease(db)
	if sc.Cost != 3 {
		t.Errorf("Decaying is not working: %d", sc.Cost)
	}

	sc.StolenPriceIncrease(db)
	if sc.Cost != 6 {
		t.Errorf("Decaying is not working: %d", sc.Cost)
	}
}
