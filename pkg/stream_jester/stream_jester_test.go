package stream_jester

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestNewSecret(t *testing.T) {
	// db := database.CreateDBConn("test_beginsounds3")
	// NewSecret(db)
}

func TestCurrentJester(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	stream_command.CreateFromName(db, "damn")
	// sj := NewSecret(db)

	res := CurrentJester(db)
	if res.PlayerID != nil {
		t.Errorf("Shouldn't have a current jester")
	}
	// p := player.CreatePlayerFromName(db, "young.thug")

	// fmt.Printf("sj = %+v\n", sj)
	// tx := db.Model(&sj).Update("player_id", p.ID)
	// if tx.Error != nil {
	// 	fmt.Printf("tx.Error = %+v\n", tx.Error)
	// }
	// res = CurrentJester(db)
	// if res.PlayerID == nil {
	// 	t.Errorf("Should have a current jester")
	// }

	// sj2 := NewSecret(db)
	// fmt.Printf("sj2 = %+v\n", sj2)
}
