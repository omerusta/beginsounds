package streamgod_router

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/economy_router"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/party"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func Route(
	ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) (<-chan string, <-chan string, <-chan string, <-chan audio_request.AudioRequest) {

	chatResults := make(chan string, 1000)
	botbotResults := make(chan string, 1000)
	websocketResults := make(chan string, 1000)
	aqs := make(chan audio_request.AudioRequest)

	go func() {
		defer close(websocketResults)
		defer close(chatResults)
		defer close(botbotResults)
		defer close(aqs)

	MsgLoop:
		for msg := range commands {
			rand.Seed(time.Now().UnixNano())

			select {
			case <-ctx.Done():
				return
			default:
				parsedCmd, _, err := parser.ParseChatMessageNoJudgements(db, &msg)
				if err != nil {
					fmt.Printf(" = %+v\n", err)
				}

				targetAmount := parsedCmd.TargetAmount
				if targetAmount < 1 {
					targetAmount = 1
				}

				if msg.Streamgod && parsedCmd.Name == "unapproveparty" {
					tx := db.Where("id = ?", parsedCmd.TargetAmount).Delete(&party.Party{})
					if tx.Error != nil {
						fmt.Printf("tx.Error = %#v\n", tx.Error)
					}
					botbotResults <- fmt.Sprintf("Party ID: %d Unapproved", parsedCmd.TargetAmount)
					continue MsgLoop
				}

				if msg.Streamgod && parsedCmd.Name == "approveparty" {
					db.Model(&party.Party{}).Where("id = ?", parsedCmd.TargetAmount).Update("approved", true)
					botbotResults <- fmt.Sprintf("Party ID: %d Approved", parsedCmd.TargetAmount)
					continue MsgLoop
				}

				if msg.Streamgod && parsedCmd.Name == "unapprovedparties" {
					var res []*party.Party
					db.Where("approved = false").Find(&res)
					fmt.Printf("\tunapproved: %+v\n", res)
					for _, p := range res {
						botbotResults <- fmt.Sprintf("%d: %s - %s", p.ID, p.Name, p.Manifesto)
					}
				}

				if parsedCmd.Name == "passthejester" {
					fmt.Println("\t $$$$ PASS THE JESTER TIME")
					jester := stream_jester.CurrentJester(db)

					if jester.PlayerID == nil {
						chatResults <- "No Current Jester"
					}

					fmt.Printf("\tjester.PlayerID = %+v\n", *jester.PlayerID)
					fmt.Printf("\tmsg.PlayerID = %+v\n", *msg.PlayerID)

					// Allow Streamgods and the Jester to Pass the Jester
					if *jester.PlayerID == *msg.PlayerID || msg.Streamgod {

						// This is for find the user
						// It looks like we aren't explictly
						// allowing users to pass the jester
						var p *player.Player
						if parsedCmd.TargetUser != "" && parsedCmd.TargetUser != msg.PlayerName {
							p = player.Find(db, parsedCmd.TargetUser)
						} else {
							players := CurrentlyChatting(db, *msg.PlayerID)
							p, err = RandomPlayer(db, players)
							if err != nil {
								fmt.Printf("err = %+v\n", err)
							}
						}

						fmt.Printf("\t$$$$ New Jester Choosen:  %+v\n", p)

						if jester.PlayerID == nil {
							tx := db.Create(&jester)
							if tx.Error != nil {
								fmt.Printf("Error creating New Jester: %+v\n", tx.Error)
							}

							chatResults <- "No Current Jester"
							// We need to create a Jester
						}

						fmt.Printf("Chosen Jester!: %v\n", p.Name)
						if err != nil {
							fmt.Printf("Error passing the Jester: %+v\n", err)
							continue MsgLoop
						}

						fmt.Printf("Old Jester: %v\n", jester)

						tx := db.Model(&jester).Update("player_id", p.ID)
						if tx.Error != nil {
							fmt.Printf("Error Updating Jester: %+v\n", tx.Error)
						}
						aq, err := soundboard.CreateAudioRequest(db, "newjester", "beginbotbot")
						if err != nil {
							fmt.Printf("Error Creating Audio Request for New Jester: %+v\n", err)
							continue MsgLoop
						}
						aqs <- *aq
						msg1 := strings.Join(stream_jester.Commands, " ")
						chatResults <- msg1
						msg := fmt.Sprintf("GlitchLit GlitchLit New Jester! @%s GlitchLit GlitchLit", p.Name)
						chatResults <- msg
					}
					continue MsgLoop
				}

				if msg.Streamgod {

					// if parsedCmd.Name == "!unapprovedparties" {
					// 	var res []*party.Party
					// 	db.Where("approved = false").Find(&res)
					// 	fmt.Printf("\tunapproved: %+v\n", res)
					// 	for _, p := range res {
					// 		botbotResults <- fmt.Sprintf("%d: %s - %s", p.ID, p.Name, p.Manifesto)
					// 	}
					// }

					if parsedCmd.Name == "artleaker" {
						fmt.Println("\nART MATT IS LEAKING CREDS")
						sj := stream_jester.NewSecret(db)
						fmt.Printf("sj = %+v\n", sj)
						continue MsgLoop
					}

					// !chaos
					if parsedCmd.Name == "chaos" {
						jester := stream_jester.CurrentJester(db)
						tx := db.Model(&jester).Update("chaos_mode", !jester.ChaosMode)
						if tx.Error != nil {
							fmt.Printf("tx.err = %+v\n", tx.Error)
						}
						websocketResults <- fmt.Sprintf("!zoombegin CHAOS MODE")
						continue MsgLoop
					}

					if parsedCmd.Name == "dropeffect" {
						fmt.Println("We are Going to Drop Effects: ", msg.PlayerName)

						// ================ //
						// We Know The User //
						// ================ //
						if parsedCmd.TargetUser != "" {
							p := player.Find(db, parsedCmd.TargetUser)

							if parsedCmd.TargetCommand != "" {
								// This removes cool points Fix that
								permissions.DropeffectForPlayer(
									db,
									parsedCmd.TargetUser,
									parsedCmd.TargetCommand,
								)
								// websocketResults <- fmt.Sprintf("@%s now has !%s",
								// 	parsedCmd.TargetUser, parsedCmd.TargetCommand)
								chatResults <- fmt.Sprintf(
									"@%s now has !%s",
									parsedCmd.TargetUser,
									parsedCmd.TargetCommand,
								)
								continue MsgLoop
							}

							// IS this choice not working right???
							choices := economy_router.RandomCommandNoCost(db, p.ID)

							var res []string
							// this should be popping off
							for i := 0; i < targetAmount; i++ {
								if len(choices) < 1 {
									continue MsgLoop
								}

								randomIndex := rand.Intn(len(choices))
								targetCommand := choices[randomIndex]
								choices = economy_router.RemoveCommand(choices, randomIndex)
								permissions.DropeffectForPlayer(
									db,
									parsedCmd.TargetUser,
									targetCommand.Name,
								)
								msg := fmt.Sprintf("@%s got !%s", parsedCmd.TargetUser, targetCommand.Name)
								botbotResults <- msg
								// results <- msg
								res = append(res, msg)
							}
							returnMsg := strings.Join(res, ", ")
							if returnMsg != "" && targetAmount < 15 {
								websocketResults <- fmt.Sprintf("@%s got SFXs: %s", parsedCmd.TargetUser, returnMsg)
							}
							continue MsgLoop
						}

						// This is only userless scenarios
						if parsedCmd.TargetCommand != "" {
							// var res []string
							targetCommand := stream_command.Find(db, parsedCmd.TargetCommand)

							players := RecentChatters(db)
						KnowCommandLoop:
							for i := 0; i < targetAmount; i++ {
								p, err := RandomPlayer(db, players)
								if err != nil {
									fmt.Printf("Error Finding Random User: %+v\n", err)
									continue KnowCommandLoop
								}
								player.AllowAccess(db, p.ID, targetCommand.ID)
								// res = append(res, fmt.Sprintf("@%s", p.Name))
								websocketResults <- fmt.Sprintf("@%s got !%s", p.Name, targetCommand.Name)
							}
							// msg = strings.Join(res, " | ")
							// if msg != "" {
							// 	results <- fmt.Sprintf("%s got !%s", msg, targetCommand.Name)
							// }
						}

						// WE DON'T HAVE A COMMAND
						if parsedCmd.TargetCommand == "" {
							// Target User
							if parsedCmd.TargetUser != "" {
								p := player.Find(db, parsedCmd.TargetUser)
								targetCommand, err := randomChoice(db, p.ID)
								if err != nil {
									fmt.Printf("err = %+v\n", err)
									continue MsgLoop
								}

								permissions.DropeffectForPlayer(
									db,
									parsedCmd.TargetUser,
									targetCommand.Name,
								)
								websocketResults <- fmt.Sprintf("@young.thug now has !%s", targetCommand.Name)
								continue MsgLoop
							}

							// For every like 10 we need to send a message
							// Need to chunk in groups of 10
							// This is the no mans land
							players := RecentChatters(db)
						Loop:
							// We could launch these if we limit them
							// to a pool
							for i := 0; i < targetAmount; i++ {
								p, err := RandomPlayer(db, players)
								if err != nil {
									fmt.Printf("Error Finding Random User: %+v\n", err)
									continue Loop
								}
								targetCommand, err := randomChoice(db, p.ID)
								if err != nil {
									fmt.Printf("Error Finding Random Choice:  %+v\n", err)
									continue Loop
								}
								player.AllowAccess(db, p.ID, targetCommand.ID)
								websocketResults <- fmt.Sprintf("@%s got !%s", p.Name, targetCommand.Name)
								// msg = strings.Join(res, " | ")
							}
							// if msg != "" {
							// 	results <- msg
							// }
							continue MsgLoop
						}

					}
				}
			}
		}

		// we need to parse the command and look for !dropeffect
	}()

	return botbotResults, websocketResults, chatResults, aqs
}

type ChatResult struct {
	ID   uint
	Name string
}

func RandomPlayer(db *gorm.DB, players []ChatResult) (*player.Player, error) {
	if len(players) < 1 {
		return &player.Player{}, errors.New("No Chatters Found")
	}
	randomIndex := rand.Intn(len(players))
	res := players[randomIndex]

	return &player.Player{
		ID:   res.ID,
		Name: res.Name,
	}, nil
}

func CurrentlyChatting(db *gorm.DB, currentPlayerID uint) []ChatResult {
	var results []ChatResult

	tx := db.Table("players").Raw(`
		SELECT
			p.id as id, p.name as name
		FROM
			chat_messages cm
		INNER JOIN
			players p
		ON
			cm.player_id = p.ID
		WHERE
			cm.created_at > (NOW() - interval '10 minute')
		AND
			p.bot = false
		AND
			p.ID != ?
		GROUP BY
			p.name, p.id;
	`, currentPlayerID).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("RecentChatters Error: %+v\n", tx.Error)
	}

	return results
}

func RecentChatters(db *gorm.DB) []ChatResult {
	var results []ChatResult

	tx := db.Table("players").Raw(`
		SELECT
			p.id as id, p.name as name
		FROM
			chat_messages cm
		INNER JOIN
			players p
		ON
			cm.player_id = p.ID
		WHERE
			cm.created_at > (NOW() - interval '24 hour')
		GROUP BY
			p.name, p.id;
	`).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("RecentChatters Error: %+v\n", tx.Error)
	}

	return results
}

// This takes a user and gives a random command
// for that user
func randomChoice(db *gorm.DB, ID uint) (*economy_router.RandoComand, error) {
	rand.Seed(time.Now().UnixNano())
	choices := economy_router.RandomCommandNoCost(db, ID)
	if len(choices) == 0 {
		return &economy_router.RandoComand{}, errors.New("No Choices!")
	}
	randomIndex := rand.Intn(len(choices))
	return &choices[randomIndex], nil
}
