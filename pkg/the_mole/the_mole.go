package the_mole

import (
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/beginbot/beginsounds/pkg/pusher"
	"gorm.io/gorm"
)

func Process(db *gorm.DB) {
	host := "us-east-1.linodeobjects.com"
	ac, err := pusher.CreateAndDeleteKeys(db, "read_write")
	if err == nil {
		// leak := fmt.Sprintf("Leak: %s %s %s", ac.Name, ac.AccessKey, ac.SecretKey)
		// s3cmd --no-mime-magic --acl-public --delete-after --recursive put build/commands/ s3://beginworld/commands/

		niceLeak := fmt.Sprintf(`s3cmd put --acl-public --access_key=%s --secret_key=%s --host-bucket=%s --host=%s INSERT_LOCAL_FILE s3://%s/INSERT_FILENAME`,
			ac.AccessKey, ac.SecretKey, host, host, ac.Name)
		// niceLeak := fmt.Sprintf(`s3cmd get --recursive  --access_key=%s --secret_key=%s --host-bucket=%s --host=%s s3://%s/`,
		fmt.Printf("niceLeak = %+v\n", niceLeak)
	}
}

// This should also leak the keys maybe?
// or Create Keys under a different name
//
// How do we make a secret?
// How do we track a winner?
// How do we give only them power for effects?
//
// Stream Jester
//   - player id
//   - secret
//   - created_at
//   - deleted_at
//
// Look up current stream Jester or Stream Gods
// or allow it for a certain amount of Mana
//
// So
//
// So What if we saved a StreamJester Here
// And Generated a Secret
func LeakSecrets(db *gorm.DB, filename string) {
	host := "us-east-1.linodeobjects.com"
	ac, _ := pusher.CreateAndDeleteKeys(db, "read_write")
	cmd := "s3cmd"
	fmt.Printf("cmd = %+v\n", cmd)

	localfile := fmt.Sprintf("tmp/%s", filename)
	// localfile := fmt.Sprintf("../../tmp/%s", filename)
	args := []string{
		"put",
		"--acl-public",
		fmt.Sprintf("--access_key=%s", ac.AccessKey),
		fmt.Sprintf("--secret_key=%s", ac.SecretKey),
		fmt.Sprintf("--host-bucket=%s", ac.BucketName),
		fmt.Sprintf("--host=%s", host),
		localfile,
		fmt.Sprintf("s3://%s/%s", ac.Name, filename),
	}

	fmt.Printf("\targs = %+v\n", args)

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
