package website_generator

import (
	"fmt"
	"os"
	"os/exec"
)

func SyncRootPage(page string) {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		fmt.Sprintf("/home/begin/code/beginsounds/build/%s.html", page),
		fmt.Sprintf("s3://beginworld/%s.html", page)}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func SyncPlayerCSS(player string) {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		fmt.Sprintf("/home/begin/code/beginsounds/assets/static/%s.css", player),
		fmt.Sprintf("s3://beginworld/assets/%s.css", player)}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

// these sync functions can be separate
func SyncCommandSite(command string) {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		fmt.Sprintf("/home/begin/code/beginsounds/build/commands/%s.html", command),
		fmt.Sprintf("s3://beginworld/commands/%s.html", command)}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func SyncPartiesSite() {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		fmt.Sprintf("/home/begin/code/beginsounds/build/parties.html"),
		fmt.Sprintf("s3://beginworld/parties.html")}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func SyncUserSite(user string) {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		fmt.Sprintf("/home/begin/code/beginsounds/build/users/%s.html", user),
		fmt.Sprintf("s3://beginworld/users/%s.html", user)}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func SyncSite() {
	cmd := "s3cmd"
	args := []string{
		"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"sync",
		"/home/begin/code/beginsounds/build/",
		"s3://beginworld/",
	}
	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
