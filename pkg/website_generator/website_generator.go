package website_generator

import (
	"fmt"
	"os"
	"text/template"

	"gitlab.com/beginbot/beginsounds/pkg/party"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

type CommandResult struct {
	Name     string
	Filename string
	Cost     uint
}

type LoverResult struct {
	Name string
}

type PlayerPage struct {
	Name         string
	Domain       string
	Extension    string
	CoolPoints   uint
	StreetCred   uint
	Lovers       []LoverResult
	CommandCount int64
	Commands     []CommandResult
}
type CommandPage struct {
	Domain   string
	Name     string
	Filename string
	Cost     uint
	Owners   []OwnerResult
}

type OwnerResult struct {
	Name string
}

func CommandOwners(db *gorm.DB, commandID uint) []OwnerResult {
	var c []OwnerResult

	db.Table("players").Raw(`
			SELECT
				p.name
			FROM
				players p
		  INNER JOIN
				commands_players c
			ON
				p.ID = c.player_id
		  WHERE
				c.stream_command_id = ?
			ORDER BY
				p.name`,
		commandID).Scan(&c)

	return c
}

// We need a custom page for this
// How can we handle calling this for tests
func CreateCommandPage(db *gorm.DB, command *stream_command.StreamCommand) {
	tmpl, err := template.ParseFiles("../../templates/command.html")
	// tmpl, err := template.ParseFiles("templates/command.html")
	if err != nil {
		fmt.Println("\tError Parsing Template File: ", err)
		return
	}
	// buildFile := fmt.Sprintf("build/commands/%s.html", command.Name)
	buildFile := fmt.Sprintf("../../build/commands/%s.html", command.Name)
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}

	owners := CommandOwners(db, command.ID)
	page := CommandPage{
		Domain:   "https://beginworld.us-east-1.linodeobjects.com",
		Name:     command.Name,
		Filename: command.Filename,
		Cost:     uint(command.Cost), Owners: owners,
	}

	err = tmpl.Execute(f, page)
	if err != nil {
		fmt.Printf("Error Executing Template File: %s", err)
		return
	}
	fmt.Println("We generated a Page: ", command.Name)
}

func CommandsAndCosts(db *gorm.DB, playerID uint) []CommandResult {
	var c []CommandResult

	db.Table("stream_commands").Raw(`
			SELECT
				sc.name, sc.filename, sc.cost
			FROM
				stream_commands sc
		  INNER JOIN
				commands_players c
			ON
				sc.ID = c.stream_command_id
		  WHERE
				c.player_id = ?
			ORDER BY
				sc.name`,
		playerID).Scan(&c)

	return c
}

func LoverNames(db *gorm.DB, playerId uint) []LoverResult {
	var results []LoverResult

	tx := db.Table("players").Raw(`
		SELECT l.name FROM players p
		INNER JOIN players_lovers pl ON p.ID = pl.player_id
		INNER JOIN players l ON pl.lover_id = l.id
		WHERE p.id = ?`, playerId).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("Error LoverResult: %+v\n", tx.Error)
	}

	return results
}

type PartiesPage struct {
	Domain    string
	Extension string
	Parties   []party.PartiesAndMembers
}

func CreatePartiesPage(db *gorm.DB) {
	tmpl, _ := template.ParseFiles("templates/parties.html", "templates/sub_templates.tmpl")

	buildFile := fmt.Sprintf("build/parties.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}

	// parties := party.AllParties(db)
	parties := party.AllPartiesAndMembers(db)
	page := PartiesPage{
		Parties:   parties,
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
	}

	err = tmpl.Execute(f, page)
	if err != nil {
		fmt.Printf("Error Executing Template File: %s", err)
		return
	}
}

func CreateUserPage(db *gorm.DB, p *player.Player) {
	tmpl, err := template.ParseFiles("templates/user.html", "templates/sub_templates.tmpl")
	// tmpl, err := template.ParseFiles("../../templates/user.html")
	if err != nil {
		fmt.Println("\nErrror Parsing Template File: ", err)
		return
	}

	// buildFile := fmt.Sprintf("../../build/%s.html", p.Name)
	buildFile := fmt.Sprintf("build/users/%s.html", p.Name)
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}

	lovers := LoverNames(db, p.ID)
	fmt.Printf("\tlovers = %+v\n", lovers)
	cmds := CommandsAndCosts(db, p.ID)

	var cmdCount int64
	res := db.Raw(`
			SELECT count(*) FROM commands_players c
			WHERE c.player_id = ?
	`, p.ID).Count(&cmdCount)
	if res.Error != nil {
		fmt.Printf("res.Error = %+v\n", res.Error)
	}

	page := PlayerPage{
		Name:         p.Name,
		Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:    ".html",
		CoolPoints:   uint(p.CoolPoints),
		StreetCred:   uint(p.StreetCred),
		Lovers:       lovers,
		CommandCount: cmdCount,
		Commands:     cmds,
	}
	err = tmpl.Execute(f, page)
	if err != nil {
		fmt.Printf("Error Executing Template File: %s", err)
		return
	}
	fmt.Println("We generated a Page: ", p.Name)
}
