package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	obsws "github.com/christopher-dG/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/cmd/obs"
)

// Thing to check out
// obsws.NewAddFilterToSourceRequest
// obsws.NewAddSceneItemRequest
// obsws.NewCreateSceneRequest
// obsws.NewDeleteSceneItemRequest
// obsws.NewDuplicateSceneItemRequest
// obsws.NewGetCurrentSceneCollectionRequest
// obsws.NewGetSceneItemPropertiesRequest
// obsws.NewAddFilterToSourceRequest()
// obsws.NewResetSceneItemRequest
// obsws.NewSetSceneItemPositionRequest

func aBunchTrash(c *obsws.Client, mutex *sync.Mutex, sceneName string) {
	obs.ReturnToNormie(c, mutex)
	// x := 0.0
	// x := 0.0
	// y := 750.0
	obs.ToggleSource(c, mutex, "codin", "beginboy", true)
	obs.ToggleSource(c, mutex, "codin", "beginboy", true)
	obs.RiseSource(c, mutex, "codin", "beginboy", 750.0, 300.0, 0.0)
	// sourceName := "beginboy"
	sourceName := "artmattcardflat"
	obs.ToggleSource(c, mutex, "codin", sourceName, true)

	// color, _ := strconv.ParseFloat(obs.GREEN, 64)
	// obs.OutlineColor(&c, mutex, color, "artmattcardflat")
	// obs.PulsatingHighlight(&c, mutex, "artmattcardflat", 50, 15, false)

	obs.ToggleSource(c, mutex, "codin", "artmattfloppycard", true)
	go obs.AppearFromThinAir(c, mutex, "artmattfloppycard")
	obs.ZoomFieldOfView(c, mutex, "artmattcardflat")
	obs.ToggleSource(c, mutex, "codin", "JesterCode", true)

	<-time.NewTicker(time.Second * 5).C
	// obs.ToggleSourceFilter(&c, mutex, sourceName, "transform", true)
	// obs.ZoomAndSpin3(&c, sourceName, 5, mutex)
	// obs.ZoomAndSpin2(&c, sourceName, 10, mutex)

	// So if climb up
	// obs.SetSourcePosition(&c, mutex, "codin", "beginboy", x, y)
	// This duplicates across Scenes
	// obsws.NewDuplicateSceneItemRequest()
	// obsws.NewGetSceneItemListRequest
	// obsws.NewResetSceneItemRequest
	// Lets download some images
	// and make them avaialbe
	// and have some effects on them
	// "melkeyussr.png"
	// CreateSource(&c, mutex, "codin", "fake2")
	// CreateImageSource(&c, mutex, "codin", "melkeyussr", sourceFile)
	// <-time.NewTimer(time.Hour).C
	// obs.FetchSourceSettings(&c, mutex, "melkman")
	// artmattfloppycard goes from 90 to 0

}

func updateSceneItemProperties(c *obsws.Client, mutex *sync.Mutex, sceneName string) {
	sceneProps := obsws.NewGetSceneItemPropertiesRequest("codin", "rms2", "", 146)
	mutex.Lock()
	if err := sceneProps.Send(*c); err != nil {
		log.Print(err)
	}
	resp, _ := sceneProps.Receive()
	mutex.Unlock()

	// How can I change this???
	fmt.Printf("resp.SourceHeight = %+v\n", resp.SourceHeight)
	fmt.Printf("resp = %+v\n", resp.CropTop)

	req := obsws.NewSetSceneItemPropertiesRequest(
		"codin", // sceneName string,
		"rms2",  // item interface{},
		"rms2",  // itemName string,
		146,
		resp.PositionX,
		resp.PositionY,
		resp.PositionAlignment,
		resp.Rotation+90,
		resp.ScaleX,
		resp.ScaleY,
		resp.CropTop,
		resp.CropBottom,
		resp.CropLeft,
		resp.CropRight,
		true,  // visible
		false, // locked
		resp.BoundsType,
		resp.BoundsAlignment,
		resp.BoundsX,
		resp.BoundsY)

	mutex.Lock()
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp1, _ := req.Receive()
	fmt.Printf("resp1 = %+v\n", resp1)

}

func fetchAllSceneItems(c *obsws.Client, mutex *sync.Mutex, sceneName string) {
	req := obsws.NewGetSceneItemListRequest("codin")
	if err := req.Send(*c); err != nil {
		log.Print(err)
	}
	resp, _ := req.Receive()
	for _, s := range resp.SceneItems {
		fmt.Printf("s = %+v\n\n", s)
	}
}

// CreateSource - Create a Media Source
// TwoD23D This is the start of the Yugio Effect
func TwoD23D(c *obsws.Client, mutex *sync.Mutex, sourceName string) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	done := make(chan bool)
	go func() {
		for x := 90.0; x > 0.0; x -= 1.7 {
			select {
			case <-done:
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				filterSettings := map[string]interface{}{
					"Filter.Transform.Rotation.X": x,
				}
				req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
				mutex.Lock()
				if err := req.Send(*c); err != nil {
					log.Print(err)
				}
				resp, err := req.Receive()
				mutex.Unlock()
				if err != nil {
					log.Print(err)
				}
				fmt.Printf("resp = %+v\n", resp)
			}
		}
	}()
}
