package main

import (
	"log"
	"sync"

	obsws "github.com/christopher-dG/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/cmd/obs"
)

func main() {
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}
	if err := c.Connect(); err != nil {
		log.Print(err)
	}
	defer c.Disconnect()

	// obs.ScaleDownSource(&c, mutex, "codin", "rms2", 0.2)
	// obs.ScaleDownSource(&c, mutex, "codin", "linusjobs", 0.4)
	obs.RotateSource(&c, mutex, "codin", "linusjobs", 90.0)

	// req := obsws.NewSetSceneItemTransformRequest(
	// 	"codin", "rms2", 0.2, 0.2, 0.0,
	// )
	// mutex.Lock()
	// if err := req.Send(c); err != nil {
	// 	log.Print(err)
	// }

	// resp, _ := req.Receive()
	// fmt.Printf("resp = %+v\n", resp)

	// mutex.Unlock()
	return

}
