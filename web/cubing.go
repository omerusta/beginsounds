package main

import (
	"net/http"

	"gitlab.com/beginbot/beginsounds/pkg/cube_bet"
)

type CubeBetsPage struct {
	Domain    string
	Extension string
	Bets      []cube_bet.PlayerBet
}

func CubeBetsHandler(w http.ResponseWriter, r *http.Request) {
	// totalVoteCount := colorscheme.Count(db)
	// themesByCount := colorscheme.ThemesByCount(db)

	w.WriteHeader(http.StatusOK)
	bets := cube_bet.AllBets(db)

	page := CubeBetsPage{
		Domain: "http://localhost:1992",
		Bets:   bets,
	}

	cubeBetsTmpl.Execute(w, page)
}
