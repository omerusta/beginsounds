package main

import (
	"fmt"
	"net/http"
	"os"
)

type HelpPage struct {
	Domain    string
	Extension string
}

func HelpHandler(w http.ResponseWriter, r *http.Request) {
	remotePage := HelpPage{
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
	}
	buildFile := fmt.Sprintf("build/help.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = helpTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := HelpPage{
		Domain: "http://localhost:1992",
	}
	helpTmpl.Execute(w, page)
}
