package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
)

type HomePage struct {
	StylishUsers  []string
	TotalCount    int64
	Domain        string
	Extension     string
	ThemesByCount []colorscheme.ThemeCount
	Metadata
	Commands []stream_command.StreamCommand
	Players  []player.Player
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	f, err := os.Open("assets/static/")
	if err != nil {
		fmt.Printf("Error Checking User CSS: %+v\n", err)
		return
	}

	cssFiles, err := f.Readdir(0)
	if err != nil {
		fmt.Printf("Error Checking User CSS: %+v\n", err)
		return
	}

	var stylishUsers []string

	for _, css := range cssFiles {
		filename := css.Name()
		var extension = filepath.Ext(filename)
		user := filename[0 : len(filename)-len(extension)]
		stylishUsers = append(stylishUsers, user)
	}

	commands := stream_command.All(db)
	w.WriteHeader(http.StatusOK)
	var result []player.Player
	db.Table("players").Order("cool_points desc").Scan(&result)

	remotePage := HomePage{
		Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:    ".html",
		Commands:     commands,
		Players:      result,
		Metadata:     metadata,
		StylishUsers: stylishUsers,
	}
	buildFile := fmt.Sprintf("build/index.html")
	f, err = os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = indexTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := HomePage{
		Domain:       "http://localhost:1992",
		StylishUsers: stylishUsers,
		Commands:     commands,
		Players:      result,
		Metadata:     metadata,
	}
	indexTmpl.Execute(w, page)
}
