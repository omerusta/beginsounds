package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
)

type UserRequestsPage struct {
	Domain       string
	Extension    string
	UserRequests []*soundeffect_request.SoundeffectRequest
}

func UserRequestsHandler(w http.ResponseWriter, r *http.Request) {
	// We need all unapproved requests
	sfxs, _ := soundeffect_request.Unapproved(db)

	// We could include other requests here
	remotePage := UserRequestsPage{
		UserRequests: sfxs,
		Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:    ".html",
	}
	buildFile := fmt.Sprintf("build/user_requests.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = userRequestsTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := UserRequestsPage{
		Domain:       "http://localhost:1992",
		UserRequests: sfxs,
	}
	w.WriteHeader(http.StatusOK)
	userRequestsTmpl.Execute(w, page)
}
